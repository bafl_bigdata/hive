-- ANAS ABID 12/10/2019
--working p_zone.surr_customer_new file
create or replace view p_zone.surr_customer_new as
WITH
    maid AS
    (
     SELECT
            MAX(UniqueIdentity) id --5726699
       FROM
            p_zone.surr_customer_new
    ),
    abc AS
    (
        SELECT
            concat('BS:',c2.id) Unique_Identity,
            concat('BS:',trim(c2.id)) as sourcecustomerid,
            lf.id_number,   
            c2.lead_co_mne ,
            'T24' AS source_system
        FROM
            rz_dwexport.customer c2
        INNER JOIN
            rz_dwexport.customer_localref lf
        ON
            c2.id=lf.id
        UNION ALL
        --cc customer
        SELECT --CP:CardPro
            concat('CP:',trim(REPLACE(cb_customer_idno,'-','')))    Unique_Identity,
            concat('CP:',trim(REPLACE(cb_customer_idno,'-',''))) as sourcecustomerid,
            trim(REPLACE(cb_customer_idno,'-',''))                AS id_number,
            'BNK',
            'CC' AS source_system
        FROM
            rz_cardpro.cp_csttbl
        --banca customer
        UNION ALL
        SELECT --BA: Banca Assurance
            concat('BA:',x.nic) AS Unique_Identity,
            concat('BA:',nic) as sourcecustomerid,
            nic               AS id_number ,
            'BNK',
            'Banca' AS source_system
        FROM
            (
                SELECT
                DISTINCT coalesce(trim(replace(nic,'-','')),'') nic
                FROM
                    rz_banca.t_banc_customeraccount tbc
                    )x
    )
SELECT
    row_number()over(ORDER BY Unique_Identity) AS surr_cust_id,
    cc.*
FROM
    abc cc
