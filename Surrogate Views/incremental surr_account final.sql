--CREATE OR REPLACE VIEW p_zone.surr_account AS
--edited by Arsalan Chhotani on 18/12/19
-- Created by: Hamza
-- Date: 2019-12-09
--CREATE OR REPLACE VIEW p_zone.v_surr_account AS
-- drop table p_zone.surr_account
-- select * from p_zone.surr_account
--  create table p_zone.surr_account stored as orc as
 WITH maid AS (
SELECT
	MAX(surr_account_id) id
	--4265994

	FROM p_zone.surr_account ) ,
abc AS (
SELECT
	concat('BS:',
	a.lead_co_mne,
	':',
	trim(a.id)) AS SourceAccountId,
	concat('BS:',
	trim(a.customer)) AS unique_identity,
	trim(coalesce(a.id, '')) as account_num,
	'T24' AS source_system
FROM
	rz_dwexport.account a
where
	a.id <> ''
	or a.id is not null
UNION ALL
SELECT
	concat('BS:',
	FX.lead_co_mne,
	':',
	trim(FX.id)) AS SourceAccountId,
	concat('BS:',
	trim(FX.counterparty)) AS unique_identity,
	trim(FX.id) AS account_num,
	'T24' AS source_system
FROM
	rz_dwexport.FOREX FX
UNION ALL
SELECT
	concat('BS:',
	a.lead_co_mne,
	':',
	trim(a.id)) AS SourceAccountId,
	concat('BS:',
	trim(a.customer)) AS unique_identity,
	trim(a.id) AS account_num,
	'T24' AS source_system
FROM
	rz_dwexport.MD_DEAL a
UNION ALL
SELECT
	concat('BS:',
	a.lead_co_mne,
	':',
	trim(a.id)) AS SourceAccountId,
	concat('BS:',
	trim(a.customer)) AS unique_identity,
	trim(a.id) AS account_num,
	'T24' AS source_system
FROM
	rz_dwexport.MG_MORTGAGE a
UNION ALL
SELECT
	concat('BS:',
	COALESCE(a.lead_co_mne,
	''),
	':',
	trim(a.id)) AS SourceAccountId,
	concat('BS:',
	trim(a.customer_id)) AS unique_identity,
	trim(a.id) AS account_num,
	'T24' AS source_system
FROM
	rz_dwexport.MM_MONEY_MARKET a
UNION ALL
SELECT
	concat('BS:',
	COALESCE(a.lead_co_mne,
	''),
	':',
	trim(a.id)) AS SourceAccountId,
	concat('BS:',
	trim(coalesce(a.customer, ''))) AS unique_identity,
	trim(a.id) AS account_num,
	'T24' AS source_system
FROM
	rz_dwexport.PD_PAYMENT_DUE a
UNION ALL
SELECT
	concat('BS',
	COALESCE(a.lead_co_mne,
	''),
	':',
	trim(a.id)) AS SourceAccountId,
	concat('BS:',
	trim(a.counterparty)) AS unique_identity,
	trim(a.id) AS account_num,
	'T24' AS source_system
FROM
	rz_dwexport.REPO a
UNION ALL
SELECT
	concat('BS:',
	COALESCE(a.lead_co_mne,
	''),
	':',
	trim(a.id)) AS SourceAccountId,
	concat('BS:',
	trim(coalesce(a.beneficiary_custno, ''))) AS unique_identity,
	trim(a.id) AS account_num,
	'T24' AS source_system
FROM
	rz_dwexport.letter_of_credit a
UNION ALL
SELECT
	concat('BS:',
	COALESCE(a.lead_co_mne,
	''),
	':',
	a.id) AS SourceAccountId,
	concat('BS:',
	coalesce(a.customer_id,
	'')) AS unique_identity,
	trim(a.id) AS account_num,
	'T24' AS source_system
FROM
	rz_dwexport.ld_loans_and_deposits a
UNION ALL
SELECT
	concat('CP:',
	'BNK',
	':',
	coalesce(cc.cb_individual_acctno,
	'')) AS SourceAccountId,
	concat('CP:',
	trim(REPLACE(cc.cb_idno, '-', ''))) AS unique_identity ,
	trim(coalesce(cc.cb_cardholder_no, '')) as account_num,
	'CC' AS source_system
FROM
	rz_cardpro.cp_crdtbl cc
UNION ALL
SELECT
	concat('BA:',
	'BNK',
	':',
	trim(a.bancappid)) AS SourceAccountId,
	concat('BA:',
	concat(trim(a.bancappid),
	'|',
	coalesce(REPLACE(a.nic,
	'-',
	''),
	''))) AS unique_identity,
	trim(a.bancappid) AS account_num,
	'Banca' AS source_system
FROM
	rz_banca.t_banc_customeraccount a )
SELECT
	maid.id + CAST(row_number() over(
ORDER BY
	ss.SourceAccountId) AS INT) AS surr_account_id,
	ss.*
FROM
	abc ss
LEFT JOIN p_zone.surr_account tt ON
	ss.sourceaccountid = tt.sourceaccountid,
	maid
WHERE
	tt.surr_account_id IS NULL;
--select max(surr_account_id) from p_zone.surr_account where account_num='1006175199' and  source_system='T24' limit 200 --3640099 3640099

