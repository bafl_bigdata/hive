create or replace view p_zone.v_account_status_hist as
with abc as 
(
select
	a.ac_category,
	a.ac_open_dt,
	a.account_num,
	a.account_status,
	a.account_title,
	ra.mis_date as cob_date,
	'T24' as source_system
from
	p_zone.account a
inner join rz_dwexport.account ra on
	a.account_num = ra.id
where
	a.account_status <> ra.record_status
)
select * from abc








