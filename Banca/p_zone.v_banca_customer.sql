CREATE or replace VIEW p_zone.v_banca_customer AS
with banca as 
(
SELECT
    REPLACE(trim(t.nic),
    '-',
    '') AS id_number,
    t.applicantname AS full_name ,
    t.applicantname AS Short_name,
    '' AS firstName,
    '' AS lastName, 
    t.contact AS Home_phone ,
    t.salutation AS Title ,
    t.dob as src_dob,
    cast(from_unixtime(unix_timestamp(t.dob, 'yyyy-MM-dd'), 'yyyy-MM-dd') as date) AS dob,
    t.address AS Home_address , 
    t.branchid AS branch ,
    t.account ,
    t.bancappid ,
    t.clientid ,
    t.branchname ,
    t.branchmanagername ,
    t.branchcontact ,
    t.policyissuedate as src_policyissuedate,
    from_unixtime(unix_timestamp(t.policyissuedate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS policyissuedate,
    t.policyissueno ,
    t.policydate as src_policydate,
    from_unixtime(unix_timestamp(t.policydate, 'yyyy-MM-dd'), 'yyyy-MM-dd') as policydate,
    t.maturityyear ,
    t.maturitydate as src_maturitydate,
    from_unixtime(unix_timestamp(t.maturitydate , 'yyyy-MM-dd'), 'yyyy-MM-dd') as maturitydate,
    t.ptdbenefitaccount ,
    t.insuredamount ,
    t.installmentamt ,
    t.beneficiaryname ,
    t.bmid ,
    t.bmname ,
    t.bmemail ,
    t.status as cust_status ,
    t.issupervised ,
    t.superviseddate as src_superviseddate,
    from_unixtime(unix_timestamp(t.superviseddate, 'yyyy-MM-dd'), 'yyyy-MM-dd') as superviseddate,
    t.loginname ,
    t.isrejected ,
    t.frequencyid ,
    t.contributionduedate as src_contributionduedate,
    from_unixtime(unix_timestamp(t.contributionduedate, 'yyyy-MM-dd'), 'yyyy-MM-dd') as contributionduedate,
    t.productid ,
    t.revisiondate as src_revisiondate,
    from_unixtime(unix_timestamp(t.revisiondate, 'yyyy-MM-dd'), 'yyyy-MM-dd') as revisiondate,
    t.revisionsupervisedate as src_revisionsupervisedate,    
    from_unixtime(unix_timestamp(t.revisionsupervisedate, 'yyyy-MM-dd'), 'yyyy-MM-dd') as revisionsupervisedate,
    t.ishold ,
    t.holddate as src_holddate,
    from_unixtime(unix_timestamp(t.holddate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS holddate,
    t.releasedate as src_releasedate,
    from_unixtime(unix_timestamp(t.releasedate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS releasedate,
    t.cancellationdate as src_cancellationdate,
    from_unixtime(unix_timestamp(t.cancellationdate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS cancellationdate,
    t.lapsedate as src_lapsedate,
    from_unixtime(unix_timestamp(t.lapsedate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS lapsedate,
    t.refstaffid ,
    t.refstaffname ,
    t.refstaffemail ,
    t.bscid ,
    t.bscname ,
    t.bscemail ,
    t.apprefid ,
    t.alfalahplan ,
    t.insurerid ,
    t.planid ,
    t.isrenwalemail ,
    t.fapamount ,
    t.fapprocessed ,
    t.noofmonth ,
    t.issarmayaprocess ,
    t.lastbranchrate ,
    t.lastbmrate ,
    t.laststaffrate ,
    t.lastbscrate ,
    t.lastfaprate ,
    t.lastsarmayarate ,
    t.lastretrate ,
    t.lastretamount ,
    t.lastcommissionamount,    
    t.lastsbrtaxamount ,
    t.lastcommissiondate as src_lastcommissiondate,
    from_unixtime(unix_timestamp(t.lastcommissiondate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS lastcommissiondate,
    t.firstrenweldate as src_firstrenewaldate,
    from_unixtime(unix_timestamp(t.firstrenweldate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS firstrenweldate,
    t.onwardrenweldate as src_onwardrenewaldate,
    from_unixtime(unix_timestamp(t.onwardrenweldate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS onwardrenweldate,
    t.istenpercent ,
    t.lastcontributiondate as src_lastcontributiondate,
    from_unixtime(unix_timestamp(t.lastcontributiondate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS lastcontributiondate,
    t.keyval ,
    t.firststatus ,
    t.firstdeductiondate as src_firstdeductiondate ,
    from_unixtime(unix_timestamp(t.firstdeductiondate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS firstdeductiondate,
    t.ft ,
    t.prevcancellation ,
    t.editstatus ,
    t.rejectedcomment ,
    t.username ,
    t.islock ,
    t.createdat as src_createdat,
    from_unixtime(unix_timestamp(t.createdat, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS createdat,
    t.effective_date as src_effective_date,
    from_unixtime(unix_timestamp(t.effective_date, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS effective_date,
    t.relationship_with_beneficiary ,
    t.cnic_of_beneficiary ,
    t.tax,
    'Banca' as source_system,
    concat('BA:',REPLACE(trim(t.nic),'-','')) AS sourcecustomerid,
    current_date  as cob_date
FROM
    rz_banca.t_banc_customeraccount t
    where t.status='P'
)
select  b.* from banca b 
