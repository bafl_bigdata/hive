CREATE or replace VIEW p_zone.v_banca_account AS with banca as (
SELECT
    trim(s.bancappid) AS Account_num ,
    concat(s.salutation,
    s.applicantname) AS Account_title,
    customer AS cust_id,
    trim(s.branchid) AS branch_code ,
    CAST(from_unixtime(unix_timestamp(a.opening_date,
    'yyyyMMdd')) AS DATE) AS Ac_open_dt ,
    a.inactiv_marker AS account_status ,
    '' AS account_type ,
    trim(replace(s.nic, '-', '')) AS id_number ,
    '' AS id_type ,
    trim(a.customer) AS customer_id ,
    a.currency ,
    a.category AS ac_category,
    '' AS balance_fcy,
    a.open_actual_bal AS balance_lcy ,
    'Banca' AS Source_system ,
    a.account_officer ,
    CAST(from_unixtime(unix_timestamp(a.date_last_cr_cust,
    'yyyyMMdd')) AS DATE) AS last_cr_cust_date ,
    a.amnt_last_cr_cust AS last_cr_amnt_cust ,
    a.tran_last_cr_cust AS last_cr_tran , 
    s.branchname ,
    s.branchcontact ,
    s.address ,
    s.dob as src_dob,
    cast(from_unixtime(unix_timestamp(s.dob, 'yyyy-MM-dd'), 'yyyy-MM-dd') as date) AS dob,
    s.contact ,
    s.policyissuedate as src_policyissuedate,
    from_unixtime(unix_timestamp(s.policyissuedate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS policyissuedate,
    s.policyissueno ,
    s.policydate as src_policydate,
    from_unixtime(unix_timestamp(s.policydate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS policydate,
    s.maturityyear ,
    s.maturitydate as src_maturitydate,
    from_unixtime(unix_timestamp(s.maturitydate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS maturitydate,
    s.bmid ,
    s.bmname ,
    s.bmemail ,
    s.status ,
    s.contributionduedate as src_contributionduedate ,
    from_unixtime(unix_timestamp(s.contributionduedate, 'yyyy-MM-dd'), 'yyyy-MM-dd') AS contributionduedate,
    s.insuredamount AS `Limit`,
    '' AS outstanding,
    s.installmentamt AS installment_amt ,
    s.productid AS source_product_id,
    s.planid AS product_id,
    concat('BA:','BNK',':', trim(s.bancappid)) as sourceaccountid,
	concat('BA:',REPLACE(trim(s.nic),'-','')) AS sourcecustomerid    
FROM
    rz_dwexport.account a
INNER JOIN rz_banca.t_banc_customeraccount s ON
    trim(s.account) = trim(a.id)
    where s.status = 'P'
    )
    select
    bb.*,
	current_date as cob_date
from
    banca as bb