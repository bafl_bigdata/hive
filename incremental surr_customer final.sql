--CREATE OR REPLACE VIEW p_zone.surr_customer AS
--edited by Arsalan Chhotani on 18/12/19
WITH
    maid AS
    (
     SELECT
            MAX(surr_cust_id) id --5726699 4396414
       FROM
            p_zone.surr_customer
    ),
    abc AS
    (
        SELECT
            concat('BS:',c2.id) Unique_Identity,
            lf.id_number,
            c2.lead_co_mne,
            'T24' AS source_system
        FROM
            rz_dwexport.customer c2
        INNER JOIN
            rz_dwexport.customer_localref lf
        ON
            c2.id=lf.id
        UNION ALL
        --cc customer
        SELECT
            concat('CRD:',trim(REPLACE(cb_customer_idno,'-','')))    Unique_Identity,
            trim(REPLACE(cb_customer_idno,'-',''))                AS id_number,
            'BNK',
            'CC' AS source_system
        FROM
            rz_cardpro.cp_csttbl
        --banca customer
        UNION ALL
        SELECT
            concat('BNC:',trim(REPLACE(x.nic,'-',''))) AS Unique_Identity,
            trim(REPLACE(x.nic,'-',''))                AS id_number ,
            'BNK',
            'Banca' AS source_system
        FROM
            (
                SELECT
                DISTINCT trim(replace(nic,'-','')) as nic
                FROM
                    rz_banca.t_banc_customeraccount tbc)x
    )
SELECT
     MAID.ID+cast(row_number()over(ORDER BY cc.Unique_Identity)AS INT) AS surr_cust_id,
    cc.*
FROM
    abc cc
LEFT JOIN p_zone.surr_customer tt ON
    cc.Unique_Identity = tt.unique_identity,
    maid
WHERE
    tt.surr_cust_id IS NULL
    
--select * from p_zone.surr_customer_new where unique_identity='BS:10256199'    