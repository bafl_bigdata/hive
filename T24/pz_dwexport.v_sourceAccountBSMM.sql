
CREATE OR REplace VIEW p_zone.v_sourceAccountBSMM AS
WITH
    acc AS
    (          
     SELECT 
            concat('BS:' , COALESCE(COM.FINANCIAL_MNE,'') , ':' , COALESCE(CAST( mm.ID AS string),''))    AS SourceAccountId ,
            concat('BS:BNK:' , COALESCE(CAST(mm.CO_CODE AS string),''))                      AS SourceBranchId  ,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(CAST(mm.CUSTOMER_ID AS string),'')) AS
            SourceCustomerId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , COALESCE(CAST(MM.MIS_ACCT_OFFICER AS string),'')) AS
                                                                  SourceEmployeeId ,
            concat('BS:' , COALESCE(com.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(l.ID,''))                 AS SourceLimitId    ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , COALESCE(CAST(mm.CATEGORY AS string),'')) AS
            SourceProductID ,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , CAST(COALESCE(mm.CUSTOMER_ID,'') AS string) , '-' ,coalesce(mm.PORTFOLIO_NUMBER,'')) AS SourcePortfolioID ,
            concat(COALESCE(CAST(COM.MNEMONIC AS string),'') , ':', 'CNS') AS SourceSystemSecurityID 
            , 
            DEAL_DATE AS Ac_open_dt 
            ,
            MM.VALUE_DATE AS OriginalStartDate ,
            CASE
                WHEN mm.STATUS = 'LIQ'
                THEN gl.MaturityDate
            END AS ClosedDate ,
            CAST(from_unixtime(unix_timestamp(CAST(maturity_date AS string), 'yyyyMMdd'),
            'yyyy-MM-dd') AS DATE) AS                     MaturityDate ,
            mm.Rollover_date       AS                     LastRenewDate 
            ,
            CASE
                WHEN gl.Category = 'Loan'
                THEN -1
                ELSE 1
            END * CAST(gl.Balance AS DECIMAL (28,4)) AS balance_lcy ,
            CASE
                WHEN gl.Category = 'Loan'
                THEN -1
                ELSE 1
            END * CAST(gl.ForeignCurrencyBal AS DECIMAL (28,4)) AS balance_fcy ,
            CASE
                WHEN gl.Category = 'Loan'
                THEN -1
                ELSE 1
            END * CAST(gl.InterestAccrued AS DECIMAL (28,4)) AS InterestAccrued ,
            CASE
                WHEN gl.Category = 'Loan'
                THEN -1
                ELSE 1
            END * CAST(gl.Authorized AS DECIMAL (28,4)) AS Authorized ,
            CASE
                WHEN COALESCE(gl.Authorized, 0 ) - COALESCE(gl.Balance, 0) > 0
                THEN COALESCE(gl.Authorized, 0 ) - COALESCE(gl.Balance, 0)
                ELSE 0
            END AS AvailableFunds 
            ,
            CASE
                WHEN mm.INTEREST_RATE IS NULL
                THEN mm.INTEREST_SPREAD_1
                ELSE mm.INTEREST_RATE
            END       AS                                  InterestRate ,
            PRINCIPAL AS                                  OriginalLoanAmount -----------ADD BY ANAS 
            ,
            mm.CURRENCY ,
            mm.ID AS Account_Num 
            ,
            CAST(mm.STATUS AS string) AS                 Account_status ,
            cBroker.NAME_1            AS                  BrokerName ,
            mm.CUSTOMER_ID            AS                  customer_id -------ADD BY ANAS 
            ,
            CASE
                WHEN INTEREST_KEY IS NOT NULL
                THEN 'variable'
                ELSE 'fixed'
            END                                   AS FixOrVar ,
            CAST (mm.INTEREST_SPREAD_1 AS FLOAT ) AS      InterestRateVariance ,
            RateIndex.DESCRIPTION                 AS      InerestRateIndex     ,
            ib.INT_BASIS                          AS      InterestBasis        ,
            mm.INTEREST_BASIS                     AS      INTEREST_BASIS
            ,
            DATEDIFF (CAST(from_unixtime(unix_timestamp(CAST(maturity_date AS string), 'yyyyMMdd'),
            'yyyy-MM-dd') AS DATE),CAST(from_unixtime(unix_timestamp(CAST(deal_date AS string),
            'yyyyMMdd') , 'yyyy-MM-dd') AS DATE)) AS      Term ,
            'd'                                   AS      TermUnit
            ,
            'Deal'        AS                              Account_Type    ,
            c.DESCRIPTION AS                              ProductDesc ,
            mm.CATEGORY   AS                              ac_category,
            mm.CATEGORY   AS                              product_id
            ,
            'BSMM'            AS                          SystemSource ,
            'MM.MONEY.MARKET' AS                          SourceApplication
            ,
            mm.ACCRUAL_PARAM                                       AS ACCRUAL_PARAM      ,
            mm.TOT_INTEREST_AMT                                     AS TOT_INTEREST_AMT   ,
            mm.INT_DUE_DATE                                         AS INT_DUE_DATE       ,
            mm.LIQ_DEFER_INTEREST                                   AS LIQ_DEFER_INTEREST ,
            mm.BROKER_CODE                                          AS BROKER_CODE        ,
            mm.BROKERAGE                                            AS BROKERAGE          ,
            mm.BROKER_CCY                                           AS BROKER_CCY         ,
            mm.BROKER_AMOUNT                                        AS BROKER_AMOUNT      ,
            mm.OUR_REMARKS                                          AS OUR_REMARKS        ,
            mm.DRAWDOWN_ACCOUNT                                     AS DRAWDOWN_ACCOUNT   ,
            mm.PRIN_LIQ_ACCT                                        AS PRIN_LIQ_ACCT      ,
            mm.PRIN_BEN_BANK_1                                      AS PRIN_BEN_BANK_1    ,
            mm.PRIN_BEN_BANK_2                                      AS PRIN_BEN_BANK_2    ,
            mm.PRIN_ADDRESS                                         AS PRIN_ADDRESS       ,
            mm.BENEF_PRIN_ACCT                                      AS BENEF_PRIN_ACCT    ,
            mm.INT_LIQ_ACCT                                         AS INT_LIQ_ACCT       ,
            mm.TAX_INTEREST_KEY                                     AS TAX_INTEREST_KEY   ,
            mm.LIMIT_REFERENCE                                      AS LIMIT_REF          ,
            mm.CHRG_LIQ_ACCT                                        AS CHRG_LIQ_ACCT      ,
            mm.CURRENCY_MARKET                                      AS CURRENCY_MARKET    ,
            mm.POSITION_TYPE                                        AS POSITION_TYPE      ,
            mm.COUNTRY_RISK                                         AS COUNTRY_RISK       ,
            mm.COUNTRY_EXPOSURE                                     AS COUNTRY_EXPOSURE   ,
            mm.ROLLOVER_MARKER                                      AS ROLLOVER_MARKER    ,
            mm.NEXT_PRIN_AMOUNT                                     AS NEW_INTEREST_RATE  ,
            mm.DEALER_DESK                                          AS DEALER_DESK        ,
            mm.INT_CCY                                              AS INT_CCY            ,
            mm.INT_CCY_MKT                                          AS INT_CCY_MKT        ,
            mm.AUTO_ROLLOVER                                        AS AUTO_ROLLOVER      ,
            mm.AUTO_CAPITALISE                                      AS AUTO_CAPITALISE    ,
            mm.AUTO_ROLL_TERM                                       AS AUTO_ROLL_TERM     ,
            mm.ROLLOVER_INT_RATE                                    AS ROLLOVER_INT_RATE  ,
            mm.LIQUIDATION_MODE                                     AS LIQUIDATION_MODE   ,
            mm.INT_PERIOD_START                                     AS INT_PERIOD_START   ,
            mm.INT_PERIOD_END                                       AS INT_PERIOD_END     ,
            mm.NEXT_INT_AMOUNT                                      AS NEXT_INT_AMOUNT    ,
            mm.NEXT_INT_TAX_AMT                                     AS NEXT_INT_TAX_AMT   ,
            mm.NEXT_PRIN_AMOUNT                                     AS NEXT_PRIN_AMOUNT   ,
            mm.ORIG_START_DATE                                      AS ORIG_START_DATE    ,
            mm.ORIG_MAT_DATE                                        AS ORIG_MAT_DATE      ,
            mm.PREV_MAT_DATE                                        AS PREV_MAT_DATE      ,
            mm.TAX_INTEREST_TYPE                                    AS TAX_INTEREST_TYPE  ,
            mm.FINAL_MATURITY                                       AS FINAL_MATURITY     ,
            mm.MATURE_AT_SOD                                        AS MATURE_AT_SOD      ,
            mm.RECORD_STATUS                                        AS RECORD_STATUS      ,
            mm.CURR_NO                                              AS CURR_NO            ,
            mm.INPUTTER                                             AS INPUTTER           ,
            mm.AUTHORISER                                           AS AUTHORISER         ,
            mm.DEPT_CODE                                            AS DEPT_CODE          ,
            mm.MIS_ACCT_OFFICER                                     AS ACCOUNT_OFFICER    ,
            CAST(substring(mm.MIS_ACCT_OFFICER,2,4) AS VARCHAR(10)) AS BRANCH_CODE        ,
            mm.DATE_TIME                                            AS DATE_TIME          ,
            mm.TOT_INT_TAX                                          AS TOT_INT_TAX        ,
            mm.CUST_REMARKS                                         AS CUST_REMARKS       ,
            mmlf.FTD_TYPE                                           AS FTD_TYPE           ,
            mmlf.FTP_MIS_RATE                                       AS FTP_MIS_RATE       ,
            mmlf.PK_MAT_DT                                          AS PK_MAT_DT          ,
            mmlf.START_DATE                                         AS LFSTART_DATE       ,
            mmlf.CUST_TAX_EXEMPT                                    AS CUST_TAX_EXEMPT    ,
            mmlf.ZAKAT_EXEMPT                                       AS ZAKAT_EXEMPT       ,
            mmlf.LR_VERSION                                         AS LR_VERSION         ,
            LMM.COMMITTED_INT                                       AS COMMITTED_INT      ,
            LMM.INT_REC_IN_ADV                                      AS INT_REC_IN_ADV     ,
            LMMDF.OUTS_CURR_PRINC                                   AS OUTS_CURR_PRINC    ,
            LMMDF.OUTS_OD_PRINC                                     AS OUTS_OD_PRINC,
            'T24'              AS                          Source_System ,
            CAST(from_unixtime(unix_timestamp(mm.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date 
       FROM
            MM_MONEY_MARKET mm
  LEFT JOIN
            COMPANY COM
         ON
            COM.MNEMONIC = MM.BRANCH_CO_MNE
  LEFT JOIN
            COMPANY_CHECK_Company_Mne CO_CHK
         ON
            CO_CHK.ID          = 'FIN.FILE'
        AND CO_CHK.Company_Mne = mm.LEAD_CO_MNE
  LEFT JOIN
            (
             SELECT
                    LINE_CONTRACT,
                    CURRENCY     ,
                    LEAD_CO_MNE  ,
                    MAX(CAST(from_unixtime(unix_timestamp(CAST(mat_Date AS string), 'yyyyMMdd'),
                    'yyyy-MM-dd') AS DATE)) AS MaturityDate,
                    MAX(at.Category)        AS Category    ,
                    MAX(INTEREST_RATE)      AS InterestRate,
                    SUM(
                        CASE
                            WHEN at.Category IS NOT NULL
                            THEN CAST(LOCAL_CCY_AMT AS DECIMAL (28,4))
                        END) AS Balance,
                    SUM(
                        CASE
                            WHEN at.Category IS NOT NULL
                            THEN CAST(CURRENCY_AMT AS DECIMAL (28,4))
                        END) AS ForeignCurrencyBal,
                    SUM(
                        CASE
                            WHEN SUBSTR(ASSET_TYPE,0, 1) = '5'
                            THEN CAST(LOCAL_CCY_AMT AS DECIMAL (28,4))
                        END) AS InterestAccrued,
                    SUM(
                        CASE
                            WHEN ASSET_TYPE    LIKE '%CONT%'
                            AND ASSET_TYPE NOT LIKE '%BL'
                            THEN CAST(LOCAL_CCY_AMT AS DECIMAL (28,4))
                        END)                                                         AS Authorized
                    ,crf.mis_date
        
               FROM
                    RE_CRF_GL crf
          LEFT JOIN
                      p_zone.at1 at
                 ON
                    crf.ASSET_TYPE = at.AssetType 
           GROUP BY
                    LEAD_CO_MNE  ,
                    LINE_CONTRACT,
                    CURRENCY,
                    mis_date ) gl

         ON
            mm.ID          = gl.LINE_CONTRACT
        AND mm.CURRENCY    = gl.CURRENCY
        AND GL.LEAD_CO_MNE = COM.FINANCIAL_MNE
  LEFT JOIN
            LIMIT l
         ON
            mm.CUSTOMER_ID +'.' + lpad ( mm.LIMIT_REFERENCE , 10,'0000') = l.ID
        AND L.LEAD_CO_MNE                                                = COM.CUSTOMER_MNEMONIC
  LEFT JOIN
            CUSTOMER cBroker
         ON
            mm.CUSTOMER_ID      = cBroker.ID
        AND cBroker.LEAD_CO_MNE = COM.CUSTOMER_MNEMONIC
            --LEFT JOIN CATEGORY c on c.ID = mm.CATEGORY AND C.LEAD_CO_MNE = CO_CHK.COMPANY_MNE
  LEFT JOIN
            CATEGORY c
         ON
            c.ID          = mm.CATEGORY
        AND c.LEAD_CO_MNE = mm.LEAD_CO_MNE
            --LEFT JOIN BASIC_RATE_TEXT RateIndex on RateIndex.ID = mm.INTEREST_KEY AND
            -- RateIndex.LEAD_CO_MNE = CO_CHK.COMPANY_MNE
  LEFT JOIN
            BASIC_RATE_TEXT RateIndex
         ON
            RateIndex.ID       = mm.INTEREST_KEY
        AND CO_CHK.Company_Mne = mm.LEAD_CO_MNE
            --LEFT JOIN INTEREST_BASIS ib on mm.INTEREST_BASIS = IB.ID AND IB.LEAD_CO_MNE =
            -- CO_CHK.COMPANY_MNE
  LEFT JOIN
            INTEREST_BASIS ib
         ON
            mm.INTEREST_BASIS  = IB.ID
        AND CO_CHK.Company_Mne = mm.LEAD_CO_MNE
  LEFT JOIN
            MM_MONEY_MARKET_LocalRef MMlf
         ON
            MMlf.ID          = MM.ID
        AND MMlf.LEAD_CO_MNE = MM.LEAD_CO_MNE 
  LEFT JOIN
            LMM_ACCOUNT_BALANCES LMM
         ON
            mm.ID           = SUBSTR(LMM.ID,0, LENGTH(mm.ID))
        AND LMM.LEAD_CO_MNE = COM.FINANCIAL_MNE -- NEED TO HANDLE LMM PROPERLY
  LEFT JOIN
            (
             SELECT
                    *
               FROM
                    (
                     SELECT
                            * ,
                            ROW_NUMBER() OVER ( PARTITION BY Temp_LMMDF.ID, Temp_LMMDF.LEAD_CO_MNE
                            ORDER BY Temp_LMMDF.Sequence DESC ) AS Rank
                       FROM
                            (
                             SELECT
                                    T_LMMDF.ID             ,
                                    T_LMMDF.LEAD_CO_MNE    ,
                                    T_LMMDF.OUTS_CURR_PRINC,
                                    T_LMMDF.OUTS_OD_PRINC  ,
                                    sequence
                               FROM
                                    LMM_ACCOUNT_BALANCES_DATE_FROM AS T_LMMDF ) AS Temp_LMMDF ) AS
                    Temp2_LMMDF
              WHERE
                    Temp2_LMMDF.Rank = 1 ) AS LMMDF
         ON
            mm.ID             = SUBSTR(LMMDF.ID,0, LENGTH(mm.ID))
        AND LMMDF.LEAD_CO_MNE = COM.FINANCIAL_MNE -- NEED TO HANDLE LMM PROPERLY
    )
select a.*, a.mis_date as cob_date 
from acc a
