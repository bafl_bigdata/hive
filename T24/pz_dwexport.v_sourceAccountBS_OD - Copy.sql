create or replace view p_zone.v_sourceAccountBS_OD as
WITH
    acc AS
    (
        SELECT
            ----------------------------------------------Source ID's Start
            -------------------------------------------------
            DISTINCT concat('BS:' , COALESCE(com.FINANCIAL_MNE,'') , ':' , CAST(a.ID AS STRING) ,
            '_OD') AS SourceAccountId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , CAST(COALESCE(A.CO_CODE,'') AS
            STRING)) AS SourceBranchId ,
            concat('BS:' , COALESCE( COM.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(CAST(C.ID AS STRING
            ),'')) AS SourceCustomerId,
            concat('BS:' , COALESCE(COALESCE(CO_CHK.COMPANY_MNE,''),'') , ':' , COALESCE(CAST
            (A.ACCOUNT_OFFICER AS STRING)),'') AS SourceEmployeeId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , COALESCE(CAST(A.ACCOUNT_OFFICER
            AS STRING)),'')                                                AS SourceOrganisationId ,
            concat('BS:' , COALESCE(com.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(l.ID,'')) AS
            SourceLimitId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , COALESCE(CAT.ID,''),'_OD') AS
                                                                       SourceProductID
            --                                                         Note:  Portfolio object is
                                                                       -- mapped in
            --                                                         Private Banking
            ,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(A.PORTFOLIO_NO,''))
                                                                           AS SourcePortfolioID ,
            concat(COALESCE(CAST(COM.MNEMONIC AS STRING),'') , ':', 'CNS') AS
                                                                            SourceSystemSecurityID
            ----------------------------------------------                  Source ID's End
            ---------------------------------------------------
            ----------------------------------------------Dates Start
            -------------------------------------------------------
            ,
            D.BusinessDate AS                             BusinessDate ,
            A.CLOSURE_DATE AS                             ClosedDate ,
            a.OPENING_DATE AS                             Ac_open_dt ,
            a.OPENING_DATE AS                             OriginalStartDate
            ----------------------------------------------Dates End
            ---------------------------------------------------------
            ----------------------------------------------Amounts Start
            -----------------------------------------------------
            ,
            CAST(COALESCE(gl.DebitBal,0) AS DECIMAL(28,4))        AS Balance ,
            CAST(COALESCE(gl.ForeignDebitBal,0) AS DECIMAL(28,4)) AS ForeignCurrencyBal ,
            CAST(COALESCE(gl.InterestAccrued,0) AS DECIMAL(28,4)) AS InterestAccrued ,
            CAST(0.0 AS DECIMAL(1,1))                             AS AvailableFunds ,
            l.INTERNAL_AMOUNT                                     AS Authorized ,
            CASE
                WHEN CAST(COALESCE(gl.DebitBal,0) AS DECIMAL(28,4)) > CAST(COALESCE(l.ONLINE_LIMIT,
                    0) AS DECIMAL(28,4))
                THEN CAST(COALESCE(gl.DebitBal,0) AS DECIMAL(28,4)) - CAST(COALESCE(l.ONLINE_LIMIT,
                    0) AS DECIMAL(28,4))
            END AS                                        OverdrawnAmount
            ----------------------------------------------Amounts End
            -------------------------------------------------------
            ----------------------------------------------Account Start
            -----------------------------------------------------
            ,
            A.ID              AS Account_Num ,
            A.CURRENCY        AS currency ,
            a.ACCOUNT_TITLE_1 AS Account_Title ,
            CASE
                WHEN gl.DebitBal > CAST(COALESCE(l.ONLINE_LIMIT,0) AS DECIMAL(28,4))
                THEN 'Yes'
                ELSE 'No'
            END AS IsOverdrawn ,
            CAST(
                CASE
                    WHEN a.CLOSURE_DATE IS NOT NULL
                    THEN 'Closed'
                    WHEN a.INACTIV_MARKER = 'Y'
                    THEN 'Inactive'
                    ELSE 'Current'
                END AS VARCHAR(20)) AS                    Account_status
            ----------------------------------------------Account End
            -------------------------------------------------------
            ----------------------------------------------Product Start (Moved to DimProduct)
            -------------------------------
            ,
            cat.ID + '_OD'               AS ac_category ,
            cat.DESCRIPTION              AS ProductDesc ,
            cat.ID + '_' +cat.SHORT_NAME AS ProductType --Assume shared products accross companies
            ,
            'Loan' AS                                     ac_category,
            ----------------------------------------------Product End (Moved to DimProduct)
            ---------------------------------
            ----------------------------------------------Traceability Start
            ------------------------------------------------
            
            a.LEAD_CO_MNE AS                              LeadCompany ,
            'T24'         AS                              Source_System ,
            'BS_OD'       AS                              SystemSource ,
            'ACCOUNT'     AS                              SourceApplication
            ----------------------------------------------Traceability End
            --------------------------------------------------
            ,
            c.id_number
        FROM
            rz_dwexport.ACCOUNT a
        INNER JOIN
            (
                SELECT
                    cl.id_number,
                    cc.id
                FROM
                    rz_dwexport.customer cc
                INNER JOIN
                    (
                        SELECT
                            selected_id_number AS id_number,
                            q.id
                        FROM
                            (
                                SELECT
                                    t1.id,
                                    t1.id_number AS idnumber1,
                                    t2.id_number,
                                    t1.sequence AS seq1,
                                    t2.sequence,
                                    CASE
                                        WHEN COALESCE(t1.id_number,'') = ''
                                        THEN t2.id_number
                                        ELSE t1.id_number
                                    END AS selected_id_number
                                FROM
                                    rz_dwexport.CUSTOMER_LocalRef_ID_TYPE t1
                                LEFT JOIN
                                    rz_dwexport.CUSTOMER_LocalRef_ID_TYPE t2
                                ON
                                    t1.id = t2.id
                                AND t2.sequence = 2
                                WHERE
                                    t1.sequence=1)q )cl
                ON
                    cc.id=cl.id )c
        ON
            c.id = a.customer
        LEFT JOIN
            rz_dwexport.COMPANY COM
        ON
            COM.MNEMONIC = A.BRANCH_CO_MNE
        LEFT JOIN
            rz_dwexport.COMPANY_CHECK_Company_Mne CO_CHK
        ON
            CO_CHK.ID = 'MASTER'
        AND CO_CHK.Sequence = 1
        LEFT JOIN
            rz_dwexport.CUSTOMER cu
        ON
            COM.CUSTOMER_MNEMONIC = Cu.LEAD_CO_MNE
        AND CAST(a.CUSTOMER AS DECIMAL(32,0)) = cu.ID
        LEFT JOIN
            rz_dwexport.CATEGORY cat
        ON
            cat.LEAD_CO_MNE =a.LEAD_CO_MNE
        AND a.CATEGORY = cat.ID
        LEFT JOIN
            rz_dwexport.`LIMIT` l
        ON
            CAST(a.CUSTOMER AS DECIMAL(32,0)) +'.' + lpad ( a.LIMIT_REF , 10,'0000') = l.ID
        AND l.LEAD_CO_MNE = COM.CUSTOMER_MNEMONIC
        LEFT JOIN
            (
                SELECT
                    LEAD_CO_MNE ,
                    LINE_CONTRACT ,
                    SUM(
                        CASE
                            WHEN ASSET_TYPE = 'DEBIT'
                            THEN CAST(COALESCE(LOCAL_CCY_AMT,0) AS DECIMAL(28,4)) * -1
                            ELSE 0
                        END) AS DebitBal ,
                    SUM(
                        CASE
                            WHEN ASSET_TYPE = 'ACCDRINTEREST'
                            THEN CAST(COALESCE(LOCAL_CCY_AMT,0) AS DECIMAL(28,4)) * -1
                            ELSE 0
                        END) AS InterestAccrued ,
                    SUM(
                        CASE
                            WHEN ASSET_TYPE = 'DEBIT'
                            THEN CAST(COALESCE(CURRENCY_AMT,0) AS DECIMAL(28,4)) * -1
                            ELSE 0
                        END) AS ForeignDebitBal
                FROM
                    rz_dwexport.RE_CRF_GL
                WHERE
                    ASSET_TYPE IN ('DEBIT',
                                   'ACCDRINTEREST')
                GROUP BY
                    LINE_CONTRACT,
                    LEAD_CO_MNE ) AS gl
        ON
            a.LEAD_CO_MNE = gl.LEAD_CO_MNE
        AND a.ID = gl.LINE_CONTRACT
        CROSS JOIN
            p_zone.v_sourceDate d
    )
SELECT
    a.*,
    aa.surr_account_id,
    surr_cus.surr_cust_id
FROM
    acc a
LEFT JOIN
    p_zone.surr_account aa
ON
    a.SourceAccountId = aa.sourceaccountid
LEFT JOIN
    p_zone.surr_customer surr_cus
ON
    a.SourceCustomerId = surr_cus.surr_cust_id