--edited by Arsalan Chhotani on 12/12/19
Create or replace View p_zone.v_sourceAccountBSMG AS
WITH
    acc AS
    (
     SELECT 
            concat('BS:' , COALESCE(COM.FINANCIAL_MNE,'') , ':' , COALESCE(CAST(MG.ID AS string),''))     AS SourceAccountId ,
            concat('BS:BNK:' , CAST(COALESCE(MG.CO_CODE,'') AS string))                      AS SourceBranchId  ,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(CAST(MG.CUSTOMER AS string),'')) AS
            SourceCustomerId ,
            concat('BS:' , COALESCE(MG.BRANCH_CO_MNE,'') , ':' , COALESCE(CAST(MG.MIS_ACCT_OFFICER AS string),'')) AS SourceEmployeeId ,
            concat('BS:' , COALESCE(com.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(LMT.ID,''))              AS SourceLimitId    ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , COALESCE(CT.ID,''))                  AS SourceProductID  ,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(MG.PORTFOLIO_NUMBER,'')) AS SourcePortfolioID
            ,
            concat(COALESCE(CAST(COM.MNEMONIC AS string),'') , ':', 'CNS') AS SourceSystemSecurityID,
            MG.MATURITY_DATE AS MaturityDate ,
            MG.VALUE_DATE    AS Ac_open_dt  ,
            CASE
                WHEN MG.CONTRACT_STATUS='LIQ'
                THEN MG.MATURITY_DATE
            END AS ClosedDate ,
            CASE
                WHEN MG.REPAYMENT_DATE IS NULL
                THEN mg.mis_date
                ELSE CAST(SUBSTR(MG.REPAYMENT_DATE,0,8) AS DATE)
            END AS                                        NextPmtDueDate
            ,
            GL.LOCAL_CCY_AMT                AS    Balance            ,
            MG.PRINCIPAL_AMOUNT             AS          OriginalLoanAmount ,
            CAST(GL.INTEREST_RATE AS FLOAT) AS            Interestrate       ,
            GL.LOCAL_CCY_AMT                AS            InterestAccrued    ,
            LMT.ADVISED_AMOUNT              AS            Authorized
            ,
            MG.ID                              AS Account_Num    ,
            MG.OUR_REMARKS                     AS Account_Title ,
            CAST(MG.CONTRACT_STATUS AS string) AS       Account_status   ,
            CAST(CUST.NAME_1 AS string)        AS         BrokerName
            ,
            MG.FIXED_VAR_IND AS FixOrVar          ,
            MG.INTEREST_KEY  AS InterestRateIndex ,
            CASE
                WHEN CAST(MG.INTEREST_SPREAD AS DOUBLE) IS NOT NULL
                THEN COALESCE(CAST(MG.INTEREST_SPREAD AS DECIMAL (28,4)), 0.00)
            END AS                                        InterestRateVariance
            ,
            MG.TERM AS                                    Term
            ,
            GL.ASSET_TYPE          AS                     Account_Type    ,
            CAST(CT.ID AS string ) AS                     ac_category ,
            ct.id AS                                      product_id ,
            CT.DESCRIPTION         AS                     ProductDesc,
             
            'BSMG'         AS                             SystemSource ,
            MG.LEAD_CO_MNE AS                             LeadCompany  ,
            'MG.MORTGAGE'  AS                             SourceApplication
            ,
            MG.CONTRACT_DATE                                   AS CONTRACT_DATE    ,
            MG.CURRENCY                                        AS CURRENCY         ,
            MG.CUSTOMER                                        AS customer_id    ,
            MG.EFFECTIVE_INT                                   AS EFFECTIVE_INT    ,
            MG.INT_LIQ_ACCOUNT                                 AS INT_LIQ_ACCT     ,
            MG.INTEREST_BASIS                                  AS INTEREST_BASIS   ,
            MG.LIMIT_REF                                       AS LIMIT_REF        ,
            MG.MORTGAGE_ACCOUNT                                AS MORTGAGE_ACCOUNT ,
            MG.DEPT_CODE                                       AS DEPT_CODE        ,
            MG.RATE_FIX_FQY                                    AS RATE_FIX_FQY     ,
            MG.REDEM_START_DATE                                AS REDEM_START_DATE ,
            MG.REDEMPTION_DATE                                 AS REDEMPTION_DATE  ,
            MG.REPAYMENT_AMOUNT                                AS REPAYMENT_AMOUNT ,
            MG.TOTAL_PRINCIPAL                                 AS TOTAL_PRINCIPAL  ,
            MG.DRAWDOWN_ACCOUNT                                AS DRAWDOWN_ACCOUNT ,
            MG.ADD_AMOUNT                                      AS ADD_AMOUNT       ,
            MG.MIS_ACCT_OFFICER                                AS ACCOUNT_OFFICER  ,
            CAST(substring(MG.MIS_ACCT_OFFICER,2,4) AS string) AS BRANCH_CODE      ,
            MG.PRIN_INCREASE                                   AS PRIN_ADDRESS     ,
            MG.ORIGINAL_MAT_DATE                               AS ORIG_MAT_DATE    ,
            MG.ADD_PAY_TYPE                                    AS DRAW_TYPE        ,
            MG.DATE_TIME                                       AS DATE_TIME        ,
            MG.OVERRIDE                                        AS OVERRIDE         ,
            MG.MIS_INTEREST_KEY                                AS MIS_INTEREST_KEY ,
            CASE
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))= '1'
                THEN 'ADAMJEE INSURANCE COMPANY LIMITED'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='10'
                THEN 'PAK KUWAIT TAKAFUL'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='11'
                THEN 'CRESCENT INSURANCE CO.'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='12'
                THEN 'UNIVERSAL INSURANCE CO.'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='13'
                THEN 'PREMIER INSURANCE CO. LTD'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='14'
                THEN 'UNITED INSURANCE CO.'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='15'
                THEN 'EAST WEST COMPANY LIMITED'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='16'
                THEN 'SAUDI PAK INSURANCE COMPANY LTD'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='17'
                THEN 'SECURITY GENERAL INSURANCE COMPANY'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='2'
                THEN 'EFU INSURANCE LTD'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='3'
                THEN 'ALFALAH INSURANCE COMPANY LIMITED'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='4'
                THEN 'ATLAS INSURANCE COMPANY LIMITED'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='5'
                THEN 'SHAHEEN INSURANCE COMPANY LIMITED'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='6'
                THEN 'NEW HAMSHIRE INSRANCE CO.'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='7'
                THEN 'NEW JUBLIEE INSURANCE COMPANY LIMITED'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='8'
                THEN 'JUPITER INSURANCE COMPANY LIMITED'
                WHEN LTRIM(RTRIM(MGlf.INS_COMP))='9'
                THEN 'MUSLIM INSURANCE COMPANY LIMITED'
                ELSE NULL
            END                AS INSUR_PARTY_NAME ,
            MGLF.INS_COMP      AS INS_COMP         ,
            MGPCC.PRIN_BALANCE AS PRIN_BALANCE     ,
            MGPCC.PRIN_CCY     AS PRIN_CCY         ,
            MGB.OTS_INTEREST   AS OTS_INTEREST     ,
            MGB.OTS_INT_ADJUST AS OTS_INT_ADJUST   ,
            MGB.OTS_SUSP_INT   AS OTS_SUSP_INT     ,
            MGLF.INST_RATE     AS INST_RATE        ,
            CASE
                WHEN MGPCC.PRIN_CCY <> 'PKR'
                THEN (CAST(MGPCC.PRIN_BALANCE AS DECIMAL(18,2))*COALESCE(T1.MID_REVAL_RATE,1) )
                ELSE COALESCE(MGPCC.PRIN_BALANCE,1)
            END * -1 AS balance_fcy 
            ,
            CASE
                WHEN MGPCC.PRIN_CCY <> 'PKR'
                THEN (CAST(MGPCC.PRIN_BALANCE AS DECIMAL(18,2))*COALESCE(T1.MID_REVAL_RATE,1) )
                ELSE COALESCE(MGPCC.PRIN_BALANCE,1)
            END * -1 AS open_cleared_bal_base 
            ,
            CASE
                WHEN MGPCC.PRIN_CCY <> 'PKR'
                THEN (CAST(MGPCC.PRIN_BALANCE AS DECIMAL(18,2))*COALESCE(T1.MID_REVAL_RATE,1) )
                ELSE COALESCE(MGPCC.PRIN_BALANCE,1)
            END * -1 AS balance_lcy,
            'T24'           AS                             Source_System,
            CAST(from_unixtime(unix_timestamp(mg.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date
       FROM
            MG_MORTGAGE AS MG
  LEFT JOIN
            COMPANY COM
         ON
            COM.MNEMONIC = MG.BRANCH_CO_MNE
  LEFT JOIN
            COMPANY_CHECK_Company_Mne CO_CHK
         ON
            CO_CHK.ID          = 'FIN.FILE'
        AND CO_CHK.Company_Mne = MG.LEAD_CO_MNE
  LEFT JOIN
            CATEGORY CT
         ON
            MG.Category    = CT.ID
        AND CT.LEAD_CO_MNE = mg.LEAD_CO_MNE
  LEFT JOIN
            (
             SELECT
                    LEAD_CO_MNE ,
                    ID          ,
                    MID_REVAL_RATE,
                    mis_date

               FROM
                    CURRENCY
              WHERE
                    CAST(substring(ID,locate('*',ID)+1,5) AS NUMERIC) = 1 ) T1
         ON
            MG.CURRENCY    = (substring(T1.ID,1,3))
        AND T1.LEAD_CO_MNE = MG.LEAD_CO_MNE 
  LEFT JOIN
            CUSTOMER CUST
         ON
            MG.BROKER_CODE   = CUST.ID
        AND CUST.LEAD_CO_MNE = COM.CUSTOMER_MNEMONIC

  LEFT JOIN
            (
             SELECT
                    GL.LINE_CONTRACT                                               ,
                    GL.LEAD_CO_MNE                                                 ,
                    GL.ASSET_TYPE                                                  ,
                    SUM(CAST(GL.LOCAL_CCY_AMT AS DECIMAL (28,4))) AS LOCAL_CCY_AMT ,
                    GL.INTEREST_RATE                                               ,
                    GL.CUSTOMER
               FROM
                    RE_CRF_GL GL
              WHERE
                    CAST(GL.LOCAL_CCY_AMT AS DOUBLE) IS NOT NULL
                AND GL.ASSET_TYPE IN ( 'DEBIT' ,
                                      'CREDIT')
           GROUP BY
                    GL.LINE_CONTRACT ,
                    GL.ASSET_TYPE    ,
                    GL.INTEREST_RATE ,
                    GL.CUSTOMER      ,
                    GL.LEAD_CO_MNE) AS GL
         ON
            COM.FINANCIAL_MNE = GL.LEAD_CO_MNE
        AND MG.ID             = GL.LINE_CONTRACT
  LEFT JOIN
            (
             SELECT
                    *
               FROM
                    (
                     SELECT
                            * ,
                            ROW_NUMBER() OVER ( PARTITION BY Temp_MGPCC.ID, Temp_MGPCC.LEAD_CO_MNE
                            ORDER BY Temp_MGPCC.Sequence ASC ) AS Rank
                       FROM
                            (
                             SELECT
                                    T_MGPCC.ID          ,
                                    T_MGPCC.LEAD_CO_MNE ,
                                    T_MGPCC.PRIN_BALANCE,
                                    T_MGPCC.PRIN_CCY    ,
                                    sequence
                               FROM
                                    MG_BALANCES_PRIN_CCY AS T_MGPCC ) AS Temp_MGPCC ) AS
                    Temp2_MGPCC
              WHERE
                    Temp2_MGPCC.Rank = 1 ) AS MGPCC
         ON
            MG.ID             = MGPCC.ID
        AND MGPCC.LEAD_CO_MNE = COM.FINANCIAL_MNE
  LEFT JOIN
            LIMIT LMT
         ON
            CO_CHK.COMPANY_MNE = lmt.LEAD_CO_MNE
        AND CAST(GL.CUSTOMER AS string)+ '.' + REPEAT('0',8-locate('.', MG.LIMIT_REF,1)) +
            MG.LIMIT_REF = lmt.ID
  LEFT JOIN
            MG_MORTGAGE_LocalRef MGlf
         ON
            MGlf.ID          = MG.ID
        AND MGlf.LEAD_CO_MNE = MG.LEAD_CO_MNE 
  LEFT JOIN
            MG_BALANCES MGB
         ON
            MGB.ID          = MG.ID
        AND MGB.LEAD_CO_MNE = MG.LEAD_CO_MNE 
    )
select a.*, a.mis_date as cob_date
from acc a