
create or REPLACE VIEW p_zone.v_sourceAccountBSPD AS
WITH
    acc AS
    (
     SELECT 
            concat('BS:' , COALESCE(COM.FINANCIAL_MNE,'') , ':' , COALESCE(CAST(PD.id AS VARCHAR(50)),'')) AS SourceAccountId
            ,
            concat('BS:' , 'BNK' , ':'       ,COALESCE(CAST(PD.CO_CODE AS VARCHAR (50)),'')) AS SourceBranchId ,
            concat('BS:' , COALESCE(com.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(CAST(PD.CUSTOMER AS VARCHAR (50)),'')) AS
                                                                    SourceCustomerId ,
            concat('BS:' , COALESCE(com.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(LMT.id,''))               AS SourceLimitId    ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , COALESCE(CAST(CT.id AS VARCHAR(50)),'')) AS
                                                                                 SourceProductID ,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(PD.PORTFOLIO_NUMBER,'')) AS SourcePortfolioID
            ,
            concat(COALESCE(CAST(COM.MNEMONIC AS VARCHAR (10)),'') , ':', 'CNS')    AS SourceSystemSecurityID ,
            d.BusinessDate                                             AS BusinessDate           ,
            PD.FINAL_DUE_DATE                                          AS AmortMatureDate        ,
            PD.START_DATE                                              AS DisburseDate           ,
            PD.START_DATE                                              AS Ac_open_dt             ,
            CAST(CAST(gl.Mat_Date AS VARCHAR(8)) AS DATE)              AS ClosedDate             ,
            PDPDD.PAYMENT_DTE_DUE                                      AS NextPmtDueDate         ,
            PDR.LastPaymentDate                                        AS LastPaymentDate        ,
            PD.START_DATE                                              AS OriginalStartDate      ,
            GL.CURRENCY_AMT                                            AS Balance                ,
            GLB.LOCAL_CCY_AMT                                          AS LocalCurrencyBal       ,
            COALESCE(CAST(PDBAT.ORIGINAL_AMT AS DECIMAL (28,4)), 0.00) AS OriginalLoanAmount     ,
            CASE
                WHEN PDBAT.AMT_TYPE = 'PR'
                THEN COALESCE(CAST(PDBAT.CURR_OS_AMT AS DECIMAL (28,4)), 0.00)
            END AS DelinquentAmount ,
            CASE
                WHEN PDBATIN.AMT_TYPE = 'IN'
                THEN COALESCE(CAST(PDBATIN.CURR_OS_AMT AS DECIMAL (28,4)), 0.00)
            END                 AS                 InterestAccrued ,
            GL.INTEREST_RATE    AS                        InterestRate    ,
            PDB.NO_DAYS_OVERDUE AS                        DelinquentDays 
            ,
            PD.id       AS Account_Num ,
            PD.CURRENCY AS currency   ,
            CASE
                WHEN PD.STATUS <> 'WOF'
                THEN 'Active'
            END AS IsActive ,
            CASE
                WHEN GLB.ASSET_TYPE = 'Debit'
                THEN
                    CASE
                        WHEN ((- 1) * CAST(GLB.DR_AMT AS DECIMAL (28,4))) > COALESCE(CAST
                            (LMT.ONLINE_LIMIT AS DECIMAL (28,4)), 0)
                        THEN 'Yes'
                    END
                ELSE 'No'
            END                            AS IsOverdrawn   ,
            ACC.ACCOUNT_TITLE_1            AS Account_Title   ,
            CAST(PD.STATUS AS VARCHAR(20)) AS Account_status      ,
            PD.INTEREST_BASIS              AS FixorVar        ,
            PD.CATEGORY                    AS LoanCode        ,
            CT.DESCRIPTION                 AS LoanDescription ,
            CASE
                WHEN GL.MAT_DATE IS NULL
                 OR LENGTH(CAST(from_unixtime(unix_timestamp(CAST(GL.MAT_DATE AS string),'yyyyMMdd'
                    ),'yyyy-MM-dd') AS string)) <> 8
                THEN '0'
                ELSE CAST(DATEDIFF(from_unixtime(unix_timestamp(CAST(GL.MIS_DATE AS string),
                    'yyyyMMdd'),'yyyy-MM-dd'),from_unixtime(unix_timestamp(CAST(GL.Value_date AS
                    string),'yyyyMMdd'),'yyyy-MM-dd')) AS INT)
            END AS Term ,
            CASE
                WHEN GL.MAT_DATE IS NULL
                 OR LENGTH(CAST(from_unixtime(unix_timestamp(CAST(GL.MAT_DATE AS string),'yyyyMMdd'
                    ),'yyyy-MM-dd') AS string)) <> 8
                THEN '0'
                ELSE CAST(months_between(from_unixtime(unix_timestamp(CAST(GL.MIS_DATE AS string),
                    'yyyyMMdd'),'yyyy-MM-dd'),from_unixtime(unix_timestamp(CAST(GL.VALUE_DATE AS
                    string),'yyyyMMdd'),'yyyy-MM-dd')) AS INT)
            END                                 AS TermInMonths ,
            'D'                                 AS TermUnit     ,
            PD.CATEGORY                         AS ac_category,
            PD.CATEGORY                         AS product_id
  ,
            CAST(CT.DESCRIPTION AS VARCHAR(80))                    AS ProductDesc       ,
            PD.CATEGORY                                            AS ProductType       ,
            'BSPD'                                                 AS SystemSource      ,
            PD.LEAD_CO_MNE                                         AS LeadCompany       ,
            'PD.PAYMENT.DUE'                                       AS SourceApplication ,
            PDB.PAYMENT_STATUS                                     AS Status_           ,
            PDB.PREVIOUS_STATUS                                    AS RECORD_STATUS     ,
            CAST(substring(PD.ACCOUNT_OFFICER,2,4) AS VARCHAR(10))            AS BRANCH_CODE       ,
            PD.ACCOUNT_OFFICER                                                AS ACCOUNT_OFFICER   ,
            PD.CUSTOMER                                                       AS customer_id       ,
            PD.ORIG_LIMIT_REF                                                 AS LIMIT_REF         ,
            PDOSA.ORIG_STLMNT_ACT                                             AS Applicant_Acc     ,
            PD.PENALTY_RATE                                                   AS ORIG_RATE         ,
            PD.REPAYMENT_ACCT                                                 AS CHRG_LIQ_ACCT     ,
            PD.TOTAL_AMT_TO_REPAY                                             AS REPAYMENT_AMOUNT  ,
            PD.DEPT_CODE                                                      AS DEPT_CODE         ,
            PD.LIMIT_AMOUNT                                                   AS LIABILITY_AMT     ,
            TRIM(CAST(PD.REPAID_AMT as STRING))                                AS REPAID_AMT        ,
            PD.TOT_OD_TYPE_AMT                                                AS TOT_OD_TYPE_AMT   ,
            PD.TOT_OVRDUE_TYPE                                                AS TOT_OVRDUE_TYPE   ,
            PDlf.LD_TYPE                                                      AS DEAL_TYPE         ,
            PD.PAY_TYPE                                                       AS DRAW_TYPE         ,
            PD.OUTSTANDING_AMT                                                AS PROV_AMT          ,
            CAST(PD.TOTAL_AMT_TO_REPAY AS DECIMAL(18,2)) * COALESCE(T1.MID_REVAL_RATE,1) AS
            balance_fcy --Ending_Ledger_Bal_Amt,
            ,
            CAST(PD.TOTAL_AMT_TO_REPAY AS DECIMAL(18,2)) * COALESCE(T1.MID_REVAL_RATE,1) AS
            open_cleared_bal_base --Ending_Cleared_Bal_Amt,
            ,
            CAST(PD.TOTAL_AMT_TO_REPAY AS DECIMAL(18,2)) * COALESCE(T1.MID_REVAL_RATE,1) AS
            balance_lcy --Ending_Trn_Bal_Amt
            ,
            CAST(CUST.NAME_1 AS VARCHAR(50)) AS BrokerName
            ,pd.id, 
            'T24'                                                   AS Source_System,      
            CAST(from_unixtime(unix_timestamp(pd.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date
       FROM
            PD_PAYMENT_DUE AS PD
  LEFT JOIN
            PD_PAYMENT_DUE_LocalRef PDlf
         ON
            PDlf.id          = PD.id
        AND PDlf.LEAD_CO_MNE = PD.LEAD_CO_MNE 
  LEFT JOIN
            COMPANY COM
         ON
            COM.MNEMONIC = PD.BRANCH_CO_MNE
  LEFT JOIN
            CUSTOMER CUST
         ON
            PD.CUSTOMER      = CUST.id
        AND CUST.LEAD_CO_MNE = COM.CUSTOMER_MNEMONIC
  LEFT JOIN
            (
             SELECT
                    LEAD_CO_MNE ,
                    id          ,
                    MID_REVAL_RATE,
                    MIS_DATE
               FROM
                    CURRENCY
              WHERE
                    CAST(substring(id,locate('*',id)+1,5) AS NUMERIC) = 1 ) T1
         ON
            PD.CURRENCY    = (substring(T1.id,1,3))
        AND T1.LEAD_CO_MNE = PD.LEAD_CO_MNE 
  LEFT JOIN
            COMPANY_CHECK_Company_Mne CO_CHK
         ON
            CO_CHK.id          = 'FIN.FILE'
        AND CO_CHK.Company_Mne = pd.LEAD_CO_MNE
  LEFT JOIN
            RE_CRF_GL GL
         ON
            PD.id          = GL.LINE_CONTRACT
        AND GL.LEAD_CO_MNE = COM.FINANCIAL_MNE
        AND GL.ASSET_TYPE IN ( 'DEBIT' ,
                              'CREDIT' )
  LEFT JOIN
            (
             SELECT
                    GLB.LEAD_CO_MNE   ,
                    GLB.LINE_CONTRACT ,
                    GLB.ASSET_TYPE    ,
                    SUM(
                        CASE
                            WHEN GLB.ASSET_TYPE = 'DEBIT'
                            THEN CAST(COALESCE(GLB.LOCAL_CCY_AMT,0) AS DECIMAL (28,4))
                            ELSE 0
                        END) AS DR_AMT ,
                    SUM(
                        CASE
                            WHEN GLB.ASSET_TYPE = 'CREDIT'
                            THEN CAST(COALESCE(GLB.LOCAL_CCY_AMT,0) AS DECIMAL (28,4))
                            ELSE 0
                        END)                                                   AS CR_AMT ,
                    SUM(CAST(COALESCE(GLB.LOCAL_CCY_AMT,0) AS DECIMAL (28,4))) AS LOCAL_CCY_AMT
               FROM
                    RE_CRF_GL GLB
              WHERE
                    GLB.ASSET_TYPE IN ( 'DEBIT' ,
                                       'CREDIT' )
           GROUP BY
                    GLB.LEAD_CO_MNE   ,
                    GLB.LINE_CONTRACT ,
                    GLB.ASSET_TYPE ) AS GLB
         ON
            PD.id           = GLB.LINE_CONTRACT
        AND GLB.LEAD_CO_MNE = COM.FINANCIAL_MNE
  LEFT JOIN
            CATEGORY AS CT
         ON
            PD.CATEGORY    = CT.id
        AND CT.LEAD_CO_MNE = pd.LEAD_CO_MNE
  LEFT JOIN
            LIMIT LMT
         ON
            CONCAT(PD.CUSTOMER ,'.' , LPAD ( PD.LIMIT_REFERENCE , 10,'0000')) = LMT.id
        AND LMT.LEAD_CO_MNE                                           = COM.CUSTOMER_MNEMONIC
  LEFT JOIN
            (
             SELECT
                    *
               FROM
                    (
                     SELECT
                            LEAD_CO_MNE                      ,
                            split(Temp_PDB.id, '-')[0] AS id ,
                            from_unixtime(unix_timestamp(CAST(Temp_PDB.RECORD_START_DATE AS string)
                            , 'yyyyMMdd'),'yyyy-MM-dd') AS RECORD_START_DATE ,
                            ROW_NUMBER() OVER ( PARTITION BY split(Temp_PDB.id, '-')[0] ORDER BY
                            from_unixtime(unix_timestamp(CAST(Temp_PDB.RECORD_START_DATE AS string)
                            , 'yyyyMMdd'),'yyyy-MM-dd') DESC ) AS Rank ,
                            Temp_PDB.NO_DAYS_OVERDUE                   ,
                            Temp_PDB.PAYMENT_STATUS                    ,
                            Temp_PDB.PREVIOUS_STATUS 
                       FROM
                            PD_BALANCES AS Temp_PDB ) AS T
              WHERE
                    T.Rank = 1 ) AS PDB
         ON
            PD.id           = PDB.id
        AND PDB.LEAD_CO_MNE = COM.FINANCIAL_MNE
  LEFT JOIN
            (
             SELECT
                    Temp2_PDR.*
               FROM
                    (
                     SELECT
                            Temp_PDR.* ,
                            ROW_NUMBER() OVER ( PARTITION BY Temp_PDR.id, temp_pdr.lead_co_mne
                            ORDER BY LastPaymentDate DESC ) AS Rank
                       FROM
                            (
                             SELECT
                                    split(Temp_PDR.id, '-')[0] AS id,
                                    Temp_PDR.LEAD_CO_MNE            ,
                                    split(Temp_PDR.id, '-')[1] AS LastPaymentDate
                               FROM
                                    PD_REPAYMENT AS Temp_PDR ) AS Temp_PDR) AS Temp2_PDR
              WHERE
                    Temp2_PDR.Rank = 1 ) AS PDR
         ON
            PD.id           = PDR.id
        AND PDR.LEAD_CO_MNE = COM.FINANCIAL_MNE 
  LEFT JOIN
            (
             SELECT
                    *
               FROM
                    (
                     SELECT
                            * ,
                            ROW_NUMBER() OVER ( PARTITION BY Temp_PDOSA.id, Temp_PDOSA.LEAD_CO_MNE
                            ORDER BY Temp_PDOSA.Sequence DESC ) AS Rank
                       FROM
                            (
                             SELECT
                                    T_PDOSA.id              ,
                                    T_PDOSA.LEAD_CO_MNE     ,
                                    T_PDOSA.ORIG_STLMNT_ACT ,
                                    sequence
                               FROM
                                    PD_PAYMENT_DUE_ORIG_STLMNT_ACT AS T_PDOSA ) AS Temp_PDOSA ) AS
                    Temp2_PDOSA
              WHERE
                    Temp2_PDOSA.Rank = 1 ) AS PDOSA
         ON
            PD.id             = PDOSA.id
        AND PDOSA.LEAD_CO_MNE = COM.FINANCIAL_MNE
  LEFT JOIN
            ACCOUNT ACC
         ON
            PDOSA.ORIG_STLMNT_ACT = ACC.id
        AND ACC.LEAD_CO_MNE       = COM.FINANCIAL_MNE
  LEFT JOIN
            (
             SELECT
                    *
               FROM
                    (
                     SELECT
                            * ,
                            ROW_NUMBER() OVER ( PARTITION BY Temp_PDPDD.id,Temp_PDPDD.LEAD_CO_MNE
                            ORDER BY Temp_PDPDD.Sequence DESC ) AS Rank
                       FROM
                            (
                             SELECT
                                    T_PDPDD.id              ,
                                    T_PDPDD.LEAD_CO_MNE     ,
                                    T_PDPDD.PAYMENT_DTE_DUE ,
                                    sequence
                               FROM
                                    PD_PAYMENT_DUE_PAYMENT_DTE_DUE AS T_PDPDD ) AS Temp_PDPDD ) AS
                    Temp2_PDPDD
              WHERE
                    Temp2_PDPDD.Rank = 1 ) AS PDPDD
         ON
            PD.id             = PDPDD.id
        AND PDPDD.LEAD_CO_MNE = COM.FINANCIAL_MNE
  LEFT JOIN
            (
             SELECT
                    *
               FROM
                    (
                     SELECT
                            Temp_PDBAT.LEAD_CO_MNE             ,
                            split(Temp_PDBAT.id, '-')[0] AS id ,
                            ROW_NUMBER() OVER ( PARTITION BY split(Temp_PDBAT.id, '-')[0] ,AMT_TYPE
                            ORDER BY split(Temp_PDBAT.id, '-')[1] DESC ) AS Rank ,
                            Temp_PDBAT.AMT_TYPE                                  ,
                            Temp_PDBAT.ORIGINAL_AMT                              ,
                            Temp_PDBAT.CURR_OS_AMT
                       FROM
                            PD_BALANCES_AMT_TYPE Temp_PDBAT
                      WHERE
                            Temp_PDBAT.AMT_TYPE IN ('PR') ) AS Temp2_PDBAT
              WHERE
                    Temp2_PDBAT.Rank = 1 ) AS PDBAT
         ON
            PD.id             = PDBAT.id
        AND PDBAT.LEAD_CO_MNE = COM.FINANCIAL_MNE
  LEFT JOIN
            (
             SELECT
                    *
               FROM
                    (
                     SELECT
                            Temp_PDBAT.LEAD_CO_MNE             ,
                            split(Temp_PDBAT.id, '-')[0] AS id ,
                            ROW_NUMBER() OVER ( PARTITION BY split(Temp_PDBAT.id, '-')[0] ,AMT_TYPE
                            ORDER BY split(Temp_PDBAT.id, '-')[1] DESC ) AS Rank ,
                            Temp_PDBAT.AMT_TYPE                                  ,
                            Temp_PDBAT.CURR_OS_AMT
                       FROM
                            PD_BALANCES_AMT_TYPE Temp_PDBAT
                      WHERE
                            Temp_PDBAT.AMT_TYPE IN ('IN') ) AS Temp2_PDBAT
              WHERE
                    Temp2_PDBAT.Rank = 1 ) AS PDBATIN
         ON
            PD.id               = PDBATIN.id
        AND PDBATIN.LEAD_CO_MNE = COM.FINANCIAL_MNE
 CROSS JOIN
            p_zone.v_sourceDate d
    )
select 
a.*, a.mis_date as cob_date

from acc a 