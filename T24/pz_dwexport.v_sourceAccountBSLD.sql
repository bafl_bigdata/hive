--edited by Arsalan Chhotani on 12/12/19
--edited by ramiz surr_cust.surr_cust_id 17/12/19
CREATE OR REPLACE VIEW
    p_zone.v_sourceaccountbsld AS
SELECT
    a.*,a.mis_date as cob_date
FROM
    (
        SELECT
            CAST(concat('BS:' , COALESCE(COM.FINANCIAL_MNE, '') , ':' , COALESCE(LD.ID, '')) AS
            string)                                      AS SourceAccountId ,
            concat('BS:BNK:' , COALESCE(ld.CO_CODE, '')) AS SourceBranchId ,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC, '') , ':' , COALESCE(CAST(ld.CUSTOMER_ID
            AS string ), '') ) AS SourceCustomerId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE, '') , ':' , COALESCE(CAST
            (ld.MIS_ACCT_OFFICER AS string), '')) AS SourceEmployeeId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE, '') , ':' , COALESCE(CAST
            (ld.MIS_ACCT_OFFICER AS string), '')) AS SourceOrganisationId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE, '') , ':' , COALESCE(
                CASE
                    WHEN locate('_', ld.AUTHORISER) = 0
                    THEN ld.AUTHORISER
                    ELSE split(ld.AUTHORISER, '_')[1]
                END), '') AS SourceEmployeeId2 ,
            concat('BS:' , COALESCE(COM.FINANCIAL_MNE, '') , ':' , COALESCE(LD.DRAWDOWN_ACCOUNT, ''
            ))                                                           AS SourceLinkedLoanAcctId ,
            concat('BS:' , COALESCE(com.CUSTOMER_MNEMONIC, '') , ':' , COALESCE(LT.id, '')) AS
            SourceLimitId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE, '') , ':' , COALESCE(CAT.ID, '')) AS
            SourceProductID ,
            cat.id as product_id,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC, '') , ':' , COALESCE(LD.PORTFOLIO_NO, ''
            ))                                                              AS SourcePortfolioID ,
            concat(CAST(COALESCE(COM.MNEMONIC, '') AS string) , ':', 'CNS') AS
            SourceSystemSecurityID ,
            LD.FIN_MAT_DATE AS AmortMatureDate ,
            CASE
                WHEN LD.STATUS = 'LIQ'
                THEN CAST(CAST(gl.Mat_Date AS string) AS DATE)
            END                                         AS ClosedDate ,
            LD.VALUE_DATE                               AS DisburseDate ,
            LD.FIN_MAT_DATE                             AS MaturityDate ,
            LD.VALUE_DATE                               AS OriginalStartDate ,
            LD.VALUE_DATE                                 AS StartDate ,
            (LD.AMOUNT*-1)                                 AS Balance ,
            (LD.AMOUNT*-1) as balance_lcy,
            GL.LOCAL_CCY_AMT                            AS ForeignCurrencyBal ,
            CAST(LT.INTERNAL_AMOUNT AS DECIMAL (28, 4))    AS Authorized ,
            CAST(LT.AVAIL_AMT AS DECIMAL (28, 4))          AS AvailableFunds ,
            CAST(PD.TOTAL_AMT_TO_REPAY AS DECIMAL (28, 4)) AS DelinquentAmount ,
            CASE
                WHEN CAT.ID BETWEEN '21050' AND '21101'
                THEN LD.AMOUNT
            END                        AS DisburseAmount ,
            LMM.OUTS_ACCRUED_INT       AS InterestAccrued ,
            LD.AMOUNT                  AS OriginalLoanAmount ,
            CAST(LD.ID AS string)      AS Account_Num ,
            CAST(CUS.NAME_1 AS string) AS BrokerName ,
            --LD.CURRENCY                AS Currency ,
            T1.Currency_code                AS Currency ,
            CAST(LD.STATUS AS string)  AS StatusCode ,
            LD.ALT_ID                  AS ALT_ID ,
            CASE
                WHEN LD.INT_RATE_TYPE = '1'
                THEN 'Fixed'
                ELSE 'Floating'
            END                            AS FixOrVar ,
            SCF.FREQUENCY                  AS InterestPaidFreq ,
            LD.INTEREST_RATE               AS InterestRate ,
            COALESCE(RT.DESCRIPTION, NULL) AS InterestRateIndex ,
            CASE
                WHEN LD.INTEREST_SPREAD IS NULL
                THEN 0
                ELSE LD.INTEREST_SPREAD
            END AS InterestRateVariance ,
            GL.CURRENCY_AMT*CAST(SUBSTRING(INTS.INT_BASIS, 1, locate('/', INTS.INT_BASIS)-1) AS INT
            ) / CAST(SUBSTRING(INTS.INT_BASIS, locate('/', INTS.INT_BASIS)+ 1, LENGTH
            (INTS.INT_BASIS)- locate('/', INTS.INT_BASIS))AS INT)       AS IntIncomeOrExpense ,
            LD.CATEGORY                                                     AS LoanCode ,
            CAT.DESCRIPTION                                               AS LoanDescription ,
            SCF.FREQUENCY                                                   AS PmtFreq ,
            DATEDIFF(LD.VALUE_DATE, LD.FIN_MAT_DATE)                        AS Term ,
            CAST(months_between(LD.VALUE_DATE, LD.FIN_MAT_DATE) AS INT)     AS TermInMonths ,
            CAST(months_between(LD.MIS_DATE, LD.FIN_MAT_DATE) AS INT)       AS TermToMaturity ,
            CAST(months_between(LD.VALUE_DATE, LD.FIN_MAT_DATE) AS INT)/ 12 AS StandardizedTerm ,
            'D'                                                             AS TermUnit ,
            LD.CATEGORY                                                     AS ProductCode ,
            CAT.DESCRIPTION                                                 AS ProductDesc ,
            'BS'                                                            AS SourceSystem ,
            LD.LEAD_CO_MNE                                                  AS LeadCompany ,
            'BSLD'                                                          AS SystemSource ,
            'LD.LOANS.AND.DEPOSITS'                                         AS SourceApplication ,
            LD.CUSTOMER_ID                                                  AS CustomerNum ,
            LD.DEPT_CODE                                                    AS DEPT_CODE ,
            LD.INT_LIQ_ACCT                                                 AS INT_LIQ_ACCT ,
            LD.INTEREST_BASIS                                               AS INTEREST_BASIS ,
            LD.INTEREST_DUE_DATE                                            AS INT_DUE_DATE ,
            LD.LIMIT_REFERENCE                                              AS LIMIT_REF ,
            LD.REIMBURSE_AMOUNT                                             AS REIMBURSE_AMOUNT ,
            LD.AMOUNT_INCREASE                                              AS AMOUNT_INCREASE
            --,                                                                LD.CONTRACT_NO_  as
            --                                                                 CONTRACT_NO
            ,
            LD.CURR_NO                                           AS CURR_NO ,
            LD.DATE_TIME                                         AS DATE_TIME ,
            LD.DRAWDOWN_ACCOUNT                                  AS DRAWDOWN_ACCOUNT ,
            LD.DRAWDOWN_ISSUE_PRC                                AS DRAWDOWN_ISSUE_PRC ,
            LD.TOT_INTEREST_AMT                                  AS TOT_INTEREST_AMT ,
            LD.WALK_IN_CUST                                      AS WALK_IN_CUST ,
            LD.PRIN_LIQ_ACCT                                     AS PRIN_LIQ_ACCT ,
            LD.MIS_FUNDING_DEPT                                  AS MIS_FUNDING_DEPT ,
            LD.LIQUIDATION_MODE                                  AS LIQUIDATION_MODE ,
            LD.MIS_ACCT_OFFICER                                  AS ACCOUNT_OFFICER ,
            CAST(substring(LD.MIS_ACCT_OFFICER, 2, 4) AS string)                    AS BRANCH_CODE ,
            LD.INT_PAYMT_METHOD                                                AS INT_PAYMT_METHOD ,
            LD.ABC_LOAN_TYPE                                                      AS ABC_LOAN_TYPE ,
            LD.ASSIGN_DATE                                                          AS ASSIGN_DATE ,
            LD.BANK_SMARTDEAL                                                    AS BANK_SMARTDEAL ,
            LD.CUS_RATE                                                                AS CUS_RATE ,
            LD.`DATE`                                                           AS ORIG_START_DATE ,
            LD.DAYS_AMEND                                                            AS DAYS_AMEND ,
            LD.DEP_REF_ID                                                           AS REFERENCE_1 ,
            LD.DL_NO                                                                      AS DL_NO ,
            LD.DR_REF                                                               AS REFERENCE_2 ,
            LD.ERF_EFORM_NO                                                        AS ERF_EFORM_NO ,
            LD.EXPTR_DIR_IND                                                      AS EXPTR_DIR_IND ,
            LD.FTP_MIS_RATE                                                        AS FTP_MIS_RATE ,
            LD.HS_CODE                                                                  AS HS_CODE ,
            LD.LC_REF_ID                                                              AS LC_REF_ID ,
            LD.LD_REF_ID                                                              AS LD_REF_ID ,
            LD.LR_VERSION                                                            AS LR_VERSION ,
            LD.ORIGINAL_VALUE                                                    AS ORIGINAL_VALUE ,
            LD.OTHER_CONT                                                            AS OTHER_CONT ,
            LD.REST_PERIOD                                                          AS REST_PERIOD ,
            LD.VALUATION_DATE                                                    AS VALUATION_DATE ,
            LD.LOAN_TYPE                                                              AS LOAN_TYPE ,
            LD.MIS_INTEREST_RATE                                              AS MIS_INTEREST_RATE ,
            LMM.COMMITTED_INT                                                     AS COMMITTED_INT ,
            LMM.INT_REC_IN_ADV                                                   AS INT_REC_IN_ADV ,
            LMMDF.OUTS_CURR_PRINC                                               AS OUTS_CURR_PRINC ,
            LMMDF.OUTS_OD_PRINC                                                   AS OUTS_OD_PRINC ,
            CAST(LMMDF.OUTS_CURR_PRINC AS DECIMAL(28, 2)) * COALESCE(T1.MID_REVAL_RATE, 1) AS
              balance_fcy
            ,
            CAST(LMMDF.OUTS_CURR_PRINC AS DECIMAL(28, 2)) * COALESCE(T1.MID_REVAL_RATE, 1) + CAST
            (LMMDF.OUTS_OD_PRINC AS DECIMAL (28, 2)) AS open_cleared_bal_base
            ,
            CAST(LMMDF.OUTS_CURR_PRINC AS DECIMAL(28, 2)) * COALESCE(T1.MID_REVAL_RATE, 1) AS
              open_actual_bal_base
            ,
            LD.INTEREST_KEY AS MIS_INTEREST_KEY ,
            LD.LD_TYPE      AS FTD_TYPE ,
            row_number() over (partition BY CAST(concat('BS:' , COALESCE(COM.FINANCIAL_MNE,'') ,':'
            , COALESCE(LD.ID,'')) AS string) ORDER BY CAST(concat('BS:' , COALESCE
            (COM.FINANCIAL_MNE,'') ,':' , COALESCE(LD.ID,'')) AS string) ASC) AS rnk,
            'T24' as source_system,
            LD.mis_date
        FROM
            (
                SELECT
                    T1.ID ,
                    T1.LEAD_CO_MNE ,
                    T1.BRANCH_CO_MNE ,
                    CATEGORY ,
                    CAST(COALESCE(T2.AMOUNT, 0) AS DECIMAL (18, 4)) AS AMOUNT ,
                    STATUS ,
                    CURRENCY ,
                    DRAWDOWN_ACCOUNT ,
                    INTEREST_KEY ,
                    LIMIT_REFERENCE ,
                    CUSTOMER_ID ,
                    BROKER_CODE ,
                    INTEREST_BASIS ,
                    INT_RATE_TYPE ,
                    CAST(COALESCE(T3.INTEREST_RATE, 0) AS FLOAT) AS INTEREST_RATE ,
                    INTEREST_SPREAD ,
                    CAST(from_unixtime(unix_timestamp(CAST(VALUE_DATE AS string),'yyyyMMdd')) AS
                    DATE) VALUE_DATE ,
                    CO_CODE ,
                    t4.INPUTTER ,
                    MIS_ACCT_OFFICER ,
                    AUTHORISER ,
                    T1.PORTFOLIO_NO ,
                    CASE
                        WHEN LENGTH(FIN_MAT_DATE)= 8
                        THEN CAST(from_unixtime(unix_timestamp(CAST(FIN_MAT_DATE AS string),
                            'yyyyMMdd')) AS DATE)
                        ELSE T1.MIS_DATE
                    END AS FIN_MAT_DATE ,
                    t1.OUR_REMARKS ,
                    t5.ALT_ID ,
                    T1.DEPT_CODE ,
                    T1.INT_LIQ_ACCT ,
                    T1.INTEREST_DUE_DATE ,
                    T1.REIMBURSE_AMOUNT ,
                    T1.AMOUNT_INCREASE ,
                    T1.CURR_NO ,
                    T1.DATE_TIME ,
                    T1.DRAWDOWN_ISSUE_PRC ,
                    T1.TOT_INTEREST_AMT ,
                    T1.WALK_IN_CUST ,
                    T1.PRIN_LIQ_ACCT ,
                    T1.MIS_FUNDING_DEPT ,
                    T1.LIQUIDATION_MODE ,
                    T5.ABC_LOAN_TYPE ,
                    T5.ASSIGN_DATE ,
                    T5.BANK_SMARTDEAL ,
                    T5.CUS_RATE ,
                    T5.`DATE` ,
                    T5.DAYS_AMEND ,
                    T5.DEP_REF_ID ,
                    T5.DL_NO ,
                    T5.DR_REF ,
                    T5.ERF_EFORM_NO ,
                    T5.EXPTR_DIR_IND ,
                    T5.FTP_MIS_RATE ,
                    T5.HS_CODE ,
                    T5.LC_REF_ID ,
                    T5.LD_REF_ID ,
                    T5.LR_VERSION ,
                    T5.ORIGINAL_VALUE ,
                    T5.OTHER_CONT ,
                    T5.REST_PERIOD ,
                    T5.VALUATION_DATE ,
                    T5.LOAN_TYPE ,
                    T1.INT_PAYMT_METHOD ,
                    T1.MIS_INTEREST_RATE ,
                    T5.LD_TYPE,
                    CAST(from_unixtime(unix_timestamp(t1.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date
                FROM
                    rz_dwexport.LD_LOANS_AND_DEPOSITS T1
                LEFT JOIN
                    rz_dwexport.LD_LOANS_AND_DEPOSITS_amount T2
                ON
                    T1.id = T2.id
                AND T1.LEAD_CO_MNE = T2.LEAD_CO_MNE
                AND T2.sequence = 1
                LEFT JOIN
                    rz_dwexport.LD_LOANS_AND_DEPOSITS_interest_rate T3
                ON
                    T1.id = T3.id
                AND T1.LEAD_CO_MNE = T3.LEAD_CO_MNE
                AND T3.sequence = 1
                LEFT JOIN
                    rz_dwexport.LD_LOANS_AND_DEPOSITS_inputter T4
                ON
                    T1.id = T4.id
                AND T1.LEAD_CO_MNE = T4.LEAD_CO_MNE
                AND T4.sequence = 1
                LEFT JOIN
                    rz_dwexport.LD_LOANS_AND_DEPOSITS_LocalRef T5
                ON
                    T1.id = T5.id
                AND T1.LEAD_CO_MNE = T5.LEAD_CO_MNE) LD
        LEFT JOIN
            rz_dwexport.COMPANY COM
        ON
            COM.MNEMONIC = LD.BRANCH_CO_MNE
        LEFT JOIN
            rz_dwexport.COMPANY_CHECK_Company_Mne CO_CHK
        ON
            CO_CHK.ID = 'FIN.FILE'
        AND CO_CHK.Company_Mne = LD.LEAD_CO_MNE
        LEFT JOIN
            rz_dwexport.RE_CRF_GL GL
        ON
            ld.id = GL.LINE_CONTRACT
        AND gl.LEAD_CO_MNE = COM.FINANCIAL_MNE
        AND GL.ASSET_TYPE = 'LIVECMT'
        LEFT JOIN
            rz_dwexport.CATEGORY CAT
        ON
            CAT.ID = LD.CATEGORY
        AND cat.LEAD_CO_MNE = LD.LEAD_CO_MNE
        LEFT JOIN
            rz_dwexport.BASIC_RATE_TEXT RT
        ON
            RT.ID = LD.INTEREST_KEY
        AND CO_CHK.Company_Mne = LD.LEAD_CO_MNE
        LEFT JOIN
            rz_dwexport.`limit` LT
        ON
            concat(LD.CUSTOMER_ID ,'.' , LPAD ( LD.LIMIT_REFERENCE , 10,'0000' )) = LT.ID
        AND CO_CHK.Company_Mne = LD.LEAD_CO_MNE
        LEFT JOIN
            rz_dwexport.CUSTOMER CUS
        ON
            LD.BROKER_CODE = CUS.ID
        AND CO_CHK.Company_Mne = LD.LEAD_CO_MNE
        LEFT JOIN
            rz_dwexport.PD_PAYMENT_DUE PD
        ON
            concat('PD' , LD.ID) = PD.ID
        AND CO_CHK.Company_Mne = PD.LEAD_CO_MNE
        LEFT JOIN
            (
                SELECT
                    T1.id AS id ,
                    T1.LEAD_CO_MNE ,
                    T1.FREQUENCY AS FREQUENCY
                FROM
                    rz_dwexport.LD_SCHEDULE_DEFINE_frequency T1
                JOIN
                    (
                        SELECT
                            id ,
                            LEAD_CO_MNE ,
                            MAX(sequence) AS sequence
                        FROM
                            rz_dwexport.LD_SCHEDULE_DEFINE_frequency
                        GROUP BY
                            ID,
                            LEAD_CO_MNE) T2
                ON
                    T1.id = T2.id
                AND T1.lead_Co_Mne = T2.LEAD_CO_MNE
                AND T1.sequence = T2.sequence) AS SCF
        ON
            LD.id = SCF.id
        AND CO_CHK.Company_Mne = SCF.LEAD_CO_MNE
        LEFT JOIN
            rz_dwexport.LMM_ACCOUNT_BALANCES LMM
        ON
            LD.ID = RPAD (LMM.ID, LENGTH(LD.ID),'0000')
        AND LMM.LEAD_CO_MNE = COM.FINANCIAL_MNE
        LEFT JOIN
            (
                SELECT
                    LEAD_CO_MNE ,
                    ID ,
                    MID_REVAL_RATE,
                    Currency_code
                FROM
                    rz_dwexport.CURRENCY
                WHERE
                    CAST(substring(ID, locate('*', id)+ 1, 5) AS INT) = 1 ) T1
        ON
            LMM.CURRENCY = (substring(T1.ID, 1, 3))
        AND T1.LEAD_CO_MNE = LMM.LEAD_CO_MNE
        LEFT JOIN
            (
                SELECT
                    *
                FROM
                    (
                        SELECT
                            * ,
                            ROW_NUMBER() OVER ( PARTITION BY Temp_LMMDF.id, Temp_LMMDF.LEAD_CO_MNE
                            ORDER BY Temp_LMMDF.Sequence DESC ) AS Rank
                        FROM
                            (
                                SELECT
                                    T_LMMDF.ID,
                                    T_LMMDF.LEAD_CO_MNE ,
                                    T_LMMDF.OUTS_CURR_PRINC,
                                    T_LMMDF.OUTS_OD_PRINC ,
                                    sequence
                                FROM
                                    rz_dwexport.LMM_ACCOUNT_BALANCES_DATE_FROM AS T_LMMDF ) AS
                            Temp_LMMDF ) AS Temp2_LMMDF
                WHERE
                    Temp2_LMMDF.Rank = 1 ) AS LMMDF
        ON
            LD.ID = rpad (LMMDF.ID, LENGTH(LD.ID),'0000')
        AND LMMDF.LEAD_CO_MNE = COM.FINANCIAL_MNE
            -- NEED TO HANDLE LMM PROPERLY
        LEFT JOIN
            rz_dwexport.INTEREST_BASIS INTS
        ON
            LD.INTEREST_BASIS = INTS.ID
        AND CO_CHK.Company_Mne = INTS.LEAD_CO_MNE) a
WHERE
    rnk = '1'