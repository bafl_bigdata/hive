
CREATE OR REPLACE VIEW p_zone.v_sourceAccountBSRP AS
WITH
    acc AS
    (
     SELECT
            concat('BS:' , COALESCE(COM.FINANCIAL_MNE,'') , ':' , CAST(COALESCE(RP.ID,'') AS string))     AS SourceAccountId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , CAST(COALESCE(RP.CO_CODE,'') AS string)) AS SourceBranchId
            ,
            concat('BS:' , coalesce(com.CUSTOMER_MNEMONIC,'') , ':' ,coalesce( LMT.id,''))             AS SourceLimitId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , CAST(COALESCE(CT.ID,'') AS string)) AS SourceProductID   ,
            concat('BS:' , coalesce(COM.CUSTOMER_MNEMONIC,'') , ':' , coalesce(RP.CUST_PORTFOLIO,''))  AS SourcePortfolioID ,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(RP.COUNTERPARTY,'') )   AS SourceCustomerID  ,
            concat(CAST(COALESCE(COM.MNEMONIC,'') AS string) , ':', 'CNS')                AS
                               SourceSystemSecurityID , 
            CASE
                WHEN RP.CONTRACT_STATUS = 'LIQ'
                THEN CAST(from_unixtime(unix_timestamp(CAST(GL.MAT_DATE AS string),'yyyyMMdd')) AS
                    TIMESTAMP)
            END AS ClosedDate ,
            CAST(from_unixtime(unix_timestamp(CAST(GL.MAT_DATE AS string),'yyyyMMdd'),'yyyy-MM-dd')
            AS DATE)      AS AmortMatureDate ,
            GL.VALUE_DATE AS DisburseDate    ,
            CAST(from_unixtime(unix_timestamp(CAST(GL.MAT_DATE AS string),'yyyyMMdd'),'yyyy-MM-dd')
            AS DATE)              AS MaturityDate       ,
            RP.TRADE_DATE         AS OriginalStartDate  ,
            GL.VALUE_DATE         AS Ac_open_dt          ,
            GL.CURRENCY_AMT       AS Balance            ,
            GL.CURRENCY_AMT       AS balance_fcy        ,
            GL.LOCAL_CCY_AMT      AS LocalCurrencyBal   ,
            GL.LOCAL_CCY_AMT      AS balance_lcy   ,
            LMT.INTERNAL_AMOUNT   AS Authorized         ,
            RPIA.InterestAccrued  AS InterestAccrued    ,
            RP.PRINCIPAL_AMOUNT_1 AS OriginalLoanAmount ,
            RP.ID                 AS Account_Num         ,
            RP.CURRENCY           AS currency           ,
            CASE
                WHEN ((- 1) * CAST(GL.LOCAL_CCY_AMT AS DECIMAL (28,4))) > COALESCE(CAST
                    (LMT.ONLINE_LIMIT AS DECIMAL (28,4)), 0)
                THEN 'Yes'
                ELSE 'No'
            END AS IsOverdrawn ,
            CASE
                WHEN RP.CONTRACT_STATUS = 'LIQ'
                THEN 'No'
            END                                     AS IsActive ,
            CAST(RP.CONTRACT_STATUS AS VARCHAR(20)) AS Account_status ,
            CAST(GL.CURRENCY_AMT AS DECIMAL (28,4))  * COALESCE(CAST(GL.INTEREST_RATE AS NUMERIC),1)
                                                          * CAST(SUBSTR(INTB.INT_BASIS,1,locate('/',
            INTB.INT_BASIS)-1) AS NUMERIC)                  / CAST(SUBSTR(INTB.INT_BASIS,locate('/',
            INTB.INT_BASIS)+1,LENGTH(CAST(INTB.INT_BASIS AS string))-locate('/', INTB.INT_BASIS))AS
            NUMERIC)            AS IntIncomeOrExpense ,
            RP.PRODUCT_CATEGORY AS LoanCode           ,
            CT.DESCRIPTION      AS LoanDescription    ,
            
            
            CASE
                WHEN GL.MAT_DATE                IS NULL
                 OR LENGTH(CAST(MAT_DATE AS string)) <> 8
                THEN '0'
                ELSE --DATEDIFF(DAY, CONVERT(DATETIME, GL.VALUE_DATE, 126), GL.MIS_DATE)
                    CAST(months_between(CAST(from_unixtime(unix_timestamp(CAST(GL.MIS_DATE AS
                    string),'yyyyMMdd')) AS DATE), CAST(from_unixtime(unix_timestamp(CAST
                    (GL.VALUE_DATE AS string),'yyyyMMdd')) AS DATE) ) AS INT)
            END AS Term ,
            CASE
                WHEN GL.MAT_DATE                IS NULL
                 OR LENGTH(CAST(MAT_DATE AS string)) <> 8
                THEN '0'
                ELSE --DATEDIFF(MONTH, CONVERT(DATETIME, GL.VALUE_DATE, 126), GL.MIS_DATE)
                    CAST(months_between(CAST(from_unixtime(unix_timestamp(CAST(GL.MIS_DATE AS
                    string),'yyyyMMdd')) AS DATE), CAST(from_unixtime(unix_timestamp(CAST
                    (GL.VALUE_DATE AS string),'yyyyMMdd')) AS DATE) ) AS INT)
            END                  AS TermInMonths      ,
            'D'                  AS TermUnit          ,
            RP.PRODUCT_CATEGORY  AS ac_category       ,
            RP.PRODUCT_CATEGORY  AS product_id       ,
            CT.DESCRIPTION       AS ProductDesc       ,
            RP.TRANS_TYPE        AS ProductType       ,
            'BSRP'               AS SystemSource      ,
            RP.LEAD_CO_MNE       AS LeadCompany       ,
            'REPO'               AS SourceApplication ,
            RP.BROKER_NO         AS BROKER_NO         ,
            RP.DIRTY_PRICE       AS DIRTY_PRICE       ,
            RP.DRAWDOWN_ACCOUNT  AS DRAWDOWN_ACCOUNT  ,
            RP.FWD_PRICE         AS FWD_PRICE         ,
            RP.GROSS_AMOUNT      AS ADD_AMOUNT        ,
            RP.INT_LIQ_ACCT      AS INT_LIQ_ACCT      ,
            RP.NEW_NOMINAL       AS NEW_NOMINAL       ,
            RP.NEW_SEC_CODE      AS NEW_SEC_CODE      ,
            RP.PRIN_LIQ_ACCT     AS PRIN_LIQ_ACCT     ,
            RP.REPO_INTEREST     AS REPO_INTEREST     ,
            RP.REPO_RATE         AS REPO_RATE         ,
            RP.REPO_TYPE         AS REPO_TYPE         ,
            RP.RT_DEAL_TYPE      AS RT_DEAL_TYPE      ,
            RP.FWD_SETTLEMNT     AS FWD_SETTLEMNT     ,
            RPLF.BRKR_RATE       AS BRKR_RATE         ,
            RPLF.BRKRAGE         AS BRKRAGE           ,
            RPLF.DEAL_METHOD     AS DEAL_METHOD       ,
            RPLF.DEAL_MODE       AS DEAL_MODE         ,
            RPLF.RP_SC_ISSUE_DAT AS RP_SC_ISSUE_DAT,
            CU.id_number,
            'T24'                 AS Source_System      ,
            CAST(from_unixtime(unix_timestamp(rp.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date  
       FROM
            REPO AS RP
            
inner join
    (
            select cc.id,cl.id_number,cc.MIS_DATE
            from rz_dwexport.customer cc
            inner join
            (
            select selected_id_number as id_number,q.id from(
                    select t1.id, t1.id_number as idnumber1,t2.id_number, t1.sequence as seq1,t2.sequence, case  when coalesce(t1.id_number,'') = '' then t2.id_number
                                                         else t1.id_number end as selected_id_number
                    from rz_dwexport.CUSTOMER_LocalRef_ID_TYPE t1
                    left join rz_dwexport.CUSTOMER_LocalRef_ID_TYPE t2
                        on t1.id = t2.id and t2.sequence = 2    
                        where  t1.sequence=1)q 
            )cl
            on cc.id=cl.id
    )cu on cu.id = RP.COUNTERPARTY 
  LEFT JOIN
            COMPANY COM
         ON
            COM.MNEMONIC = RP.BRANCH_CO_MNE
  LEFT JOIN
            COMPANY_CHECK_Company_Mne CO_CHK
         ON
            CO_CHK.ID       = 'MASTER'
        AND CO_CHK.Sequence = 1  
  LEFT JOIN
            RE_CRF_GL AS GL
         ON
            RP.MM_CONTRACT_ID = GL.LINE_CONTRACT
        AND GL.LEAD_CO_MNE    = COM.FINANCIAL_MNE
        AND ASSET_TYPE IN ('LIVEDB')
  LEFT JOIN
            CATEGORY AS CT
         ON
            CT.ID          = GL.Consol_key05
        AND CT.LEAD_CO_MNE =gl.LEAD_CO_MNE
  LEFT JOIN
            LIMIT AS LMT
         ON
            LMT.ID          = concat(RP.COUNTERPARTY , '.' , SUBSTR('000',0, 3) , RP.LIMIT_REFERENCE)
        AND LMT.LEAD_CO_MNE = COM.CUSTOMER_MNEMONIC 
  LEFT JOIN
            INTEREST_BASIS INTB
         ON
            RP.INTEREST_BASIS = INTB.ID
        AND INTB.LEAD_CO_MNE  = CO_CHK.company_mne
  LEFT JOIN
            REPO_LocalRef RPlf
         ON
            RPlf.ID          = RP.ID
        AND RPlf.LEAD_CO_MNE = RP.LEAD_CO_MNE 
  LEFT JOIN
            (
             SELECT
                    id         ,
                    LEAD_CO_MNE,
                    SUM(CAST(ACCRUED_INT_AMT AS DECIMAL (28,4))) AS InterestAccrued
               FROM
                    REPO_NEW_SEC_CODE
           GROUP BY
                    id,
                    LEAD_CO_MNE) AS RPIA
         ON
            RP.id         =RPIA.id
        AND RP.LEAD_CO_MNE=RPIA.LEAD_CO_MNE
    )
select  a.*,a.mis_date as cob_date from acc a 