
--edited by ramiz surr_cust.surr_cust_id 17/12/19
--SELECT cob_date, * from p_zone.v_sourceAccountBSFX limit 10
 CREATE Or Replace VIEW p_zone.v_sourceAccountBSFX AS with acc as (
SELECT
	concat('BS:' ,
	COALESCE(COM.FINANCIAL_MNE,
	'') ,
	':' ,
	cast(COALESCE(FX.ID,
	'') as string)) as SourceAccountId ,
	concat('BS:BNK:' ,
	cast(COALESCE(FX.CO_CODE,
	'') as string)) as SourceBranchId ,
	coalesce(fx.CO_CODE,
	'') as branch_code ,
	concat('BS:' ,COALESCE(COM.CUSTOMER_MNEMONIC,'') ,':' ,cast(COALESCE(FX.COUNTERPARTY,'') as string)) as SourceCustomerId ,
	concat('BS:' ,
	COALESCE(CO_CHK.COMPANY_MNE,
	'') ,
	':' ,
	COALESCE(fx.CATEGORY_CODE,
	'')) as SourceProductID ,
	concat(CAST(COALESCE(COM.MNEMONIC,
	'') as string) ,
	':',
	'CNS') as SourceSystemSecurityID ,
	FX.DEAL_DATE AS OriginalStartDate ,
	fx.DEAL_DATE as Ac_open_dt ,
	cast(cast(gl.MaturityDate as string) as date) as MaturityDate ,
	cast(cast
	(
		case
		when FX.STATUS = 'MAT' THEN GL.MaturityDate
END as string) as date) as ClosedDate ,
	fx.FORWARD_RATE as InterestRate ,
	fx.SPOT_RATE as ExchangeRate ,
	fx.ID as Account_Num ,
	cast(FX.STATUS as string) as Account_status ,
	c.DESCRIPTION as ProductDesc ,
	cast(c.ID as string) as ac_category ,
	cast(c.ID as string) as product_id ,
	c.DESCRIPTION as ProductType ,
	'Deal' as Account_Type ,
	FX.LEAD_CO_MNE as LeadCompany ,
	'BSFX' as SystemSource ,
	'FOREX' as SourceApplication ,
	FX.CURRENCY_BOUGHT as CURRENCY_BOUGHT ,
	FX.AMOUNT_BOUGHT as AMOUNT_BOUGHT ,
	FX.DEAL_TYPE as DEAL_TYPE ,
	FX.VALUE_DATE_BUY as VALUE_DATE_BUY ,
	FX.OUR_ACCOUNT_REC as OUR_ACCOUNT_REC ,
	FX.AMOUNT_SOLD as AMOUNT_SOLD ,
	FX.VALUE_DATE_SELL as VALUE_DATE_SELL ,
	FX.CURRENCY_SOLD as CURRENCY_SOLD ,
	FX.TRANSACTION_TYPE as TRANSACTION_TYPE ,
	FX.BROKERAGE_AMOUNT as BROKERAGE_AMOUNT ,
	FX.EQUIV_AMT as EQUIV_AMT ,
	FX.OUR_ACCOUNT_PAY as OUR_ACCOUNT_PAY ,
	FX.SPOT_DATE as SPOT_DATE ,
	FX.INPUTTER as INPUTTER ,
	FX.AUTHORISER as AUTHORISER ,
	FX.DEPT_CODE as DEPT_CODE ,
	FX.ACCOUNT_OFFICER as ACCOUNT_OFFICER ,
	FX.BUY_LCY_EQUIV as BUY_LCY_EQUIV ,
	FX.SEL_LCY_EQUIV as SEL_LCY_EQUIV ,
	FX.SPOT_LCY_AMOUNT as SPOT_LCY_AMOUNT ,
	FX.BROKERAGE as BROKERAGE ,
	FX.BASE_CCY as INT_CCY ,
	FX.BROKER as BROKER_NO ,
	FX.CPARTY_CORR_NO as CPARTY_CORR_NO ,
	FX.RECORD_STATUS as RECORD_STATUS ,
	FX.CURR_NO as CURR_NO ,
	'T24' as Source_System ,
	cu.id_number ,
	gl.balance as balance_lcy ,
	gl.ForeignCurrencyBal as balance_fcy,
	CAST(from_unixtime(unix_timestamp(fx.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date
FROM
	rz_dwexport.FOREX FX
inner join (
	select
		cl.id_number,
		cc.id
	from
		rz_dwexport.customer cc
	inner join (
		select
			selected_id_number as id_number,
			q.id
		from
			(
			select
				t1.id,
				t1.id_number as idnumber1,
				t2.id_number,
				t1.sequence as seq1,
				t2.sequence,
				case
					when coalesce(t1.id_number,
					'') = '' then t2.id_number
					else t1.id_number
			end as selected_id_number
			from
				rz_dwexport.CUSTOMER_LocalRef_ID_TYPE t1
			left join rz_dwexport.CUSTOMER_LocalRef_ID_TYPE t2 on
				t1.id = t2.id
				and t2.sequence = 2
			where
				t1.sequence = 1)q )cl on
		cc.id = cl.id )cu on
	cu.id = fx.counterparty
LEFT JOIN rz_dwexport.COMPANY COM ON
	COM.MNEMONIC = FX.BRANCH_CO_MNE
LEFT JOIN rz_dwexport.COMPANY_CHECK_Company_Mne CO_CHK ON
	CO_CHK.ID = 'FIN.FILE'
	and CO_CHK.Company_Mne = FX.LEAD_CO_MNE
LEFT JOIN (
	select
		LINE_CONTRACT,
		LEAD_CO_MNE,
		max(INTEREST_RATE) as InterestRate,
		max(MAT_DATE) as MaturityDate,
		sum(cast(coalesce(LOCAL_CCY_AMT, 0) as decimal (28, 4))) as Balance,
		sum(cast(coalesce(CURRENCY_AMT, 0) as decimal (28, 4))) as ForeignCurrencyBal
	from
		rz_dwexport.RE_CRF_GL crf
	group by
		LINE_CONTRACT,
		LEAD_CO_MNE) gl on
	FX.ID = gl.LINE_CONTRACT
	AND GL.LEAD_CO_MNE = COM.FINANCIAL_MNE
LEFT JOIN rz_dwexport.CATEGORY c on
	FX.CATEGORY_CODE = c.ID
	AND C.LEAD_CO_MNE = fx.LEAD_CO_MNE )
select
	a.*,a.mis_date as cob_date
from
	acc a