create or replace view p_zone.v_sourceAccountBS as
with acc as
(
	select concat('BS:' , COALESCE(com.FINANCIAL_MNE,'') , ':' , CAST(A.ID as string)) as SourceAccountId
	,concat('BS:BNK:' , CAST(COALESCE(A.CO_CODE,'') as string)) as SourceBranchId
	,concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , TRIM(A.CUSTOMER)) as SourceCustomerId
	,concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , CAST(COALESCE(A.ACCOUNT_OFFICER,'') as string)) as SourceEmployeeId
	,concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , CAST(COALESCE(A.ACCOUNT_OFFICER,'') as string)) as SourceOrganisationId
	,concat('BS:' , COALESCE(com.CUSTOMER_MNEMONIC,'') , ':' , a.CUSTOMER ,'.' , LPAD (  COALESCE(a.LIMIT_REF,'') , 10, '0000' ) ) as SourceLimitId
	,concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , COALESCE(a.CATEGORY,'')) as SourceProductID
	,concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(A.PORTFOLIO_NO,'')) as SourcePortfolioID
	,concat(CAST(COALESCE(COM.MNEMONIC,'') as string) , ':', 'CNS') as SourceSystemSecurityID
	,A.CLOSURE_DATE as ClosedDate  
	,CAST(from_unixtime(unix_timestamp(a.OPENING_DATE,'yyyyMMdd'),'yyyy-MM-dd') AS date) as ac_open_dt
	,a.OPENING_DATE as OriginalStartDate,
	A.ID as Account_Num
	,A.CURRENCY as Currency
	,a.ACCOUNT_TITLE_1 as Account_Title
	,cast(case	when a.CLOSURE_DATE is not null  then 'Closed' when a.INACTIV_MARKER  = 'Y' then 'Inactive' else 'Current' end as string) as Account_status			
	,a.ALT_ACCT_ID as ALT_ID
	,CAST(a.ACCOUNT_TITLE_1 AS String) As BrokerName
	,cat.ID as ac_category
	,cat.ID as product_id
	,cat.DESCRIPTION as ProductDesc
	,CONCAT(COALESCE(cat.ID,'') , '_',  COALESCE(cat.SHORT_NAME,''))  as ProductType
	,'Deposit' as Category
	,cast('BS' as string) as SystemSource
	,'BS' as SourceSystem
	, 'T24' as Source_System
	,'ACCOUNT' as SourceApplication
	, a.LEAD_CO_MNE as LeadCompany
	,a.DATE_LAST_CR_AUTO	as DATE_LAST_CR_AUTO
	,a.DATE_LAST_CR_BANK	as DATE_LAST_CR_BANK
	,a.DATE_LAST_CR_CUST	as Last_Cr_Cust_Date
	,a.DATE_LAST_DR_AUTO	as DATE_LAST_DR_AUTO
	,a.DATE_LAST_DR_BANK	as DATE_LAST_DR_BANK
	,a.DATE_LAST_DR_CUST	as Last_Dr_Cust_Date
	,a.JOINT_HOLDER			as JOINT_HOLDER
	,TRIM(a.CUSTOMER )	as Customer_id
	,a.ONLINE_ACTUAL_BAL	as ONLINE_ACTUAL_BAL
	,CAST(a.DEPT_CODE as decimal(32,0))	as DEPT_CODE
	,a.ALT_ACCT_TYPE		as ALT_ACCT_TYPE
	,a.INT_NO_BOOKING		as INT_NO_BOOKING
	,a.ONLINE_CLEARED_BAL	as ONLINE_CLEARED_BAL
	,a.INTEREST_LIQU_ACCT	as INTEREST_LIQU_ACCT
	,a.ACCR_DR_AMOUNT		as ACCR_DR_AMOUNT
	,a.LIMIT_REF			as LIMIT_REF
	,a.DATE_TIME			as DATE_TIME
	,a.ALT_ACCT_ID			as ALT_ACCT_ID
	,a.OTHER_OFFICER		as OTHER_OFFICER
	,a.OPEN_CLEARED_BAL		as OPEN_CLEARED_BAL
	,a.WORKING_BALANCE		as balance_fcy
	,a.POSTING_RESTRICT		as POSTING_RESTRICT
	,a.LOCKED_AMOUNT		as LOCKED_AMOUNT
	,a.INPUTTER				as INPUTTER
	,a.AUTHORISER			as AUTHORISER
	,a.JOINT_NOTES			as JOINT_NOTES
	,a.MNEMONIC				as MNEMONIC
	,a.AMNT_LAST_CR_AUTO	as AMNT_LAST_CR_AUTO
	,a.AMNT_LAST_CR_Cust	as Last_Cr_Amnt_Cust
	,a.Amnt_Last_DR_Auto	as Amnt_Last_DR_Auto
	,a.AMNT_LAST_DR_Cust	as Last_Dr_Amnt_Cust
	,a.TRAN_LAST_DR_CUST	as Last_Dr_Tran
	,a.ACCOUNT_OFFICER		as ACCOUNT_OFFICER
	,cast(substr(a.ACCOUNT_OFFICER,2,4) as string) as BRANCH_CODE
	,a.RECORD_STATUS		as RECORD_STATUS
	,a.OPEN_ASSET_TYPE		as OPEN_ASSET_TYPE
	,cast(a.WORKING_BALANCE as decimal(18,2)) * COALESCE(T1.MID_REVAL_RATE,1) AS balance_fcy_base
	,cast(a.OPEN_CLEARED_BAL as decimal(18,2)) * COALESCE(T1.MID_REVAL_RATE,1)  AS open_cleared_bal_base
	,cast(a.OPEN_ACTUAL_BAL as decimal(18,2))* COALESCE(T1.MID_REVAL_RATE,1) AS balance_lcy
	,a.CONDITION_GROUP		as CONDITION_GROUP
	,a.ACCT_CREDIT_INT		as ACCT_CREDIT_INT
	,a.ACCT_DEBIT_INT		as ACCT_DEBIT_INT
	,ALF.ACCT_OPR_INST_1 AS ACCT_OPR_INST_1
	,ALF.ACCT_OPR_INST_2 AS ACCT_OPR_INST_2
	,ALF.ATOGTM			 AS ATOGTM
	,ALF.ATOGTM1		 AS ATOGTM1
	,ALF.EXP_MONTH_TOVER AS EXP_MONTH_TOVER
	,ALF.KIN_ID_TYPE	 AS KIN_ID_TYPE
	,ALF.KYC_ATO		 AS KYC_ATO
	,ALF.KYC_CO_ATO		 AS KYC_CO_ATO
	,ALF.KYC_NATO		 AS KYC_NATO
	,ALF.KYC_NO_TRANS	 AS KYC_NO_TRANS
	,ALF.KYC_REASON_HIGH AS KYC_REASON_HIGH
	,ALF.MANDATEE_NAME   AS MANDATEE_NAME
	,ALF.MANDATEE_NIC    AS MANDATEE_NIC
	,ALF.MODEDEPOSITS    AS MODEDEPOSITS
	,ALF.MODEWITHDRAW    AS MODEWITHDRAW
	,ALF.MON_TOVER_CORP  AS MON_TOVER_CORP
	,ALF.MON_TOVER_CRG   AS MON_TOVER_CRG
	,ALF.MONTH_TOVER_RG  AS MONTH_TOVER_RG
	,ALF.NTN_FILER       AS NTN_FILER
	,ALF.PFAMAPPROVAL    AS PFAMAPPROVAL
	,ALF.Purpose		 AS Purpose
	,ALF.SFUNDOTH		 AS SFUNDOTH
	,ALF.SOURCEFUNDS	 AS SOURCEFUNDS
	,ALF.TOT_FIN_AMT	 AS TOT_FIN_AMT
	,ALF.UNSCLISTST		 AS UNSCLISTST
	,ALF.BAF_FATCA_CLASS AS BAF_FATCA_CLASS
	,ALF.BAF_PEN_ACCT	 AS BAF_PEN_ACCT
	,ALF.BAF_PEN_BENEFRY AS BAF_PEN_BENEFRY
	,ALF.BAF_PEN_CERTIF  AS BAF_PEN_CERTIF
	,ALF.BAF_PEN_CNIC	 AS BAF_PEN_CNIC
	,ALF.BAF_PEN_EXPIRY  AS BAF_PEN_EXPIRY
	,ALF.BAF_PEN_INSTITE AS BAF_PEN_INSTITE
	,ALF.BAF_PEN_PENCNIC AS BAF_PEN_PENCNIC
	,ALF.BAF_PEN_PNAME   AS BAF_PEN_PNAME
	,ALF.BAF_PEN_PPO	 AS BAF_PEN_PPO
	,ALF.BAF_PEN_PENDOB  AS BAF_PEN_PENDOB
	,ALF.SURRENDER_DATE  AS SURRENDER_DATE
	,ALF.UN_REASON		 AS UN_REASON
	,ALF.UNCLAIMED_SURR  AS UNCLAIMED_SURR
	,ALF.CUST_TAX_EXEMPT AS CUST_TAX_EXEMPT
	,ALF.FIQAH			 AS FIQAH
	,ALF.SBP_CATEGORY_ID AS SBP_CATEGORY_ID
	,ALF.SBP_SCAT_ID	 AS SBP_SCAT_ID
	,ALF.SBP_SECTOR_ID   AS SBP_SECTOR_ID
	,ALF.SBP_SEGMENT_ID  AS SBP_SEGMENT_ID
	,ALF.SBP_SSECTOR_ID  AS SBP_SSECTOR_ID
	,ALF.SBP_SSEGMENT_ID AS SBP_SSEGMENT_ID
	,ALF.ZAKAT_EXEMPT	 AS ZAKAT_EXEMPT
	,ALF.BS_ONLINE AS BS_ONLINE
	,ALF.CHARGES_EXEMPT	 AS CHARGES_EXEMPT
	,ALF.SBP_COMPANY	 AS SBP_COMPANY
	,ALF.CASH_TAX_EXEMPT AS CASH_TAX_EXEMPT
	,ALF.AOR AS AOR
	,ALF.bio_verisys
	,c.id_number
	,CAST(from_unixtime(unix_timestamp(A.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date
	FROM rz_dwexport.account a
	inner join
	(
	        select cl.id_number, cc.id
	        from rz_dwexport.customer cc
	        inner join
	        (
	        select selected_id_number as id_number,q.id from(
	                select t1.id, t1.id_number as idnumber1,t2.id_number, t1.sequence as seq1,t2.sequence, case  when coalesce(t1.id_number,'') = '' then t2.id_number
	                                                     else t1.id_number end as selected_id_number
	                from rz_dwexport.CUSTOMER_LocalRef_ID_TYPE t1
	                left join rz_dwexport.CUSTOMER_LocalRef_ID_TYPE t2
	                    on t1.id = t2.id and t2.sequence = 2    
	                    where  t1.sequence=1)q 
	        )cl
	        on cc.id=cl.id
	)c on c.id = a.customer
	LEFT JOIN rz_dwexport.company COM ON COM.MNEMONIC = A.BRANCH_CO_MNE
	LEFT JOIN rz_dwexport.COMPANY_CHECK_Company_Mne CO_CHK ON CO_CHK.ID = 'FIN.FILE' and CO_CHK.Company_Mne = A.LEAD_CO_MNE
	LEFT JOIN rz_dwexport.ACCOUNT_LocalRef Alf 
	ON Alf.LEAD_CO_MNE = A.LEAD_CO_MNE AND A.BRANCH_CO_MNE = ALF.BRANCH_CO_MNE AND A.MIS_DATE = ALF.MIS_DATE AND  Alf.ID = a.ID
	LEFT JOIN rz_dwexport.CATEGORY cat ON a.CATEGORY = cat.ID and a.LEAD_CO_MNE =cat.LEAD_CO_MNE 
	LEFT JOIN (SELECT LEAD_CO_MNE,ID,MID_REVAL_RATE FROM rz_dwexport.CURRENCY WHERE  cast(substr(ID,locate('*',id)+1,5) as int) = 1) T1
	ON A.CURRENCY =  (substr(T1.ID,1,3)) and T1.LEAD_CO_MNE = A.LEAD_CO_MNE
	WHERE A.ID NOT LIKE 'MG%'
)
select a.*,a.mis_date as cob_date from acc a