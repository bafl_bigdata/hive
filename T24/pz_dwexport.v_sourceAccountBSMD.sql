
CREATE or replace  VIEW p_zone.v_sourceAccountBSMD AS
WITH
    acc AS
    (
     SELECT     
            concat('BS:' , COALESCE(COM.FINANCIAL_MNE,'') , ':' , CAST(COALESCE(md.ID,'') AS string))     AS SourceAccountId ,
            concat('BS:BNK:' , COALESCE(CAST(md.CO_CODE AS string)),'')                      AS SourceBranchId  ,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(CAST(md.CUSTOMER AS string),'')) AS SourceCustomerId ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , COALESCE(CAST(md.ACCOUNT_OFFICER AS string),'')) AS SourceEmployeeId ,
            concat('BS:' , COALESCE(com.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(L.ID,''))                 AS SourceLimitId    ,
            concat('BS:' , COALESCE(CO_CHK.COMPANY_MNE,'') , ':' , COALESCE(C.ID,''))                    AS SourceProductID  ,
            concat('BS:' , COALESCE(COM.CUSTOMER_MNEMONIC,'') , ':' , COALESCE(MD.CUSTOMER,''), '-', COALESCE(MD.PORTFOLIO_NO,'')) AS SourcePortfolioID ,
            concat(COALESCE(CAST(COM.MNEMONIC AS string),'') , ':', 'CNS') AS SourceSystemSecurityID ,
            md.DEAL_DATE AS Ac_open_dt,
            MD.VALUE_DATE AS OriginalStartDate ,
            CASE
                WHEN md.STATUS ='LIQ'
                THEN CAST(CAST(gl.MaturityDate AS string) AS DATE)
            END AS ClosedDate ,
            CASE
                WHEN LENGTH(CAST(md.MATURITY_DATE AS string)) = 8
                THEN CAST(md.MATURITY_DATE AS INT)
                WHEN CAST(md.MATURITY_DATE AS INT) BETWEEN 1 AND 1000
                THEN CAST(md.MATURITY_DATE AS INT)
            END AS                                        MaturityDate
            ,
            CASE
                WHEN gl.Category = 'Loan'
                THEN -1
                ELSE 1
            END * CAST(gl.Balance AS DECIMAL (28,4)) AS Balance ,
            CASE
                WHEN gl.Category = 'Loan'
                THEN -1
                ELSE 1
            END * CAST(gl.ForeignCurrencyBal AS DECIMAL (28,4)) AS ForeignCurrencyBal ,
            CASE
                WHEN gl.Category = 'Loan'
                THEN -1
                ELSE 1
            END * CAST(gl.InterestAccrued AS DECIMAL (28,4)) AS InterestAccrued ,
            CASE
                WHEN gl.Category = 'Loan'
                THEN -1
                ELSE 1
            END * CAST(gl.Authorized AS DECIMAL (28,4)) AS Authorized
            ,
            md.CURRENCY                                   ,
            CAST(md.STATUS AS string) AS Account_status       ,
            md.ID                     AS Account_Num       ,
            md.LIQUIDATION_MODE       AS RenewalCode      ,
            md.ALTERNATE_ID           AS LegacyAccountNum ,
            md.AUTO_EXPIRY            AS IsAutoExpire
            ,
            md.ALTERNATE_ID AS                            ALT_ID
            ,
            gl.Category                         AS Category    ,
            gl.Category                         AS Group_      ,
            CAST(c.ID AS VARCHAR (50))          AS        ac_category ,
            c.id                                as        product_id  ,
            CAST(c.DESCRIPTION AS VARCHAR (50)) AS        ProductDesc ,
            md.DEAL_SUB_TYPE                    AS        ProductType
            ,
            MD.LEAD_CO_MNE AS                             LeadCompany  ,
            'BSMD'         AS                             SystemSource ,
            'MD.DEAL'      AS                             SourceApplication
            ,
            MD.BEN_ADDRESS                                 AS PRIN_ADDRESS       ,
            MD.BENEF_CUST_1                                AS BENEF_PRIN_ACCT    ,
            MD.CSN_AMOUNT                                  AS CSN_AMOUNT         ,
            MD.CUSTOMER                                    AS customer_id        ,
            MD.LIMIT_REFERENCE                             AS LIMIT_REF          ,
            MD.OUTS_PROV_AMT                               AS OUTS_PROV_AMT      ,
            MD.PRINCIPAL_AMOUNT                            AS OriginalLoanAmount ,
            MD.PROV_AMT                                    AS PROV_AMT           ,
            MD.CHARGE_CODE                                 AS CHARGE_CODE        ,
            MD.CHARGE_AMT                                  AS CHARGE_AMT         ,
            MD.CHARGE_ACCOUNT                              AS CHARGE_ACCOUNT     ,
            MD.ADVICE_EXPIRY_DATE                          AS ADVICE_EXPIRY_DATE ,
            MD.REFERENCE_1                                 AS REFERENCE_1        ,
            MD.REFERENCE_2                                 AS REFERENCE_2        ,
            MD.ACCOUNT_OFFICER                             AS ACCOUNT_OFFICER    ,
            CAST(SUBSTR(MD.ACCOUNT_OFFICER,2,4) AS string)                   AS BRANCH_CODE        ,
            MDlf.MARGIN_PERC                                                 AS WMId               ,
            CAST(MDlf.EXP_DATE AS DATE)                                      AS DATE_OF_EXP        ,
            MDlf.BLOCK_AMT                                                   AS LIABILITY_AMT      ,
            CAST(MD.PRINCIPAL_AMOUNT AS DECIMAL(18,2)) * COALESCE(T1.MID_REVAL_RATE,1) AS
            balance_fcy --Ending_Ledger_Bal_Amt
            ,
            CAST(MD.PRINCIPAL_AMOUNT AS DECIMAL(18,2)) * COALESCE(T1.MID_REVAL_RATE,1) AS
            open_cleared_bal_base --Ending_Cleared_Bal_Amt
            ,
            CAST(MD.PRINCIPAL_AMOUNT AS DECIMAL(18,2)) * COALESCE(T1.MID_REVAL_RATE,1) AS
           balance_lcy --Ending_Trn_Bal_Amt
            ,
            CAST(custowner.NAME_1 AS string) AS BrokerName,
            'T24'           AS                             Source_System, 
            CAST(from_unixtime(unix_timestamp(md.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date

       FROM
            MD_DEAL AS md
  LEFT JOIN
            COMPANY COM
         ON
            COM.MNEMONIC = MD.BRANCH_CO_MNE
  LEFT JOIN
            COMPANY_CHECK_Company_Mne CO_CHK
         ON
            CO_CHK.ID          = 'FIN.FILE'
        AND CO_CHK.Company_Mne = md.LEAD_CO_MNE
  LEFT JOIN
            MD_PARAMETER mdp
         ON
            md.CO_CODE      = mdp.ID
        AND MDP.LEAD_CO_MNE = CO_CHK.COMPANY_MNE
  LEFT JOIN
            (
             SELECT
                    LEAD_CO_MNE ,
                    ID          ,
                    MID_REVAL_RATE,
                    mis_date
               FROM
                    CURRENCY
              WHERE
                    CAST(substring(ID,instr(id,'*')+1,5) AS NUMERIC) = 1) T1
         ON
            md.CURRENCY    = (substring(T1.ID,1,3))
        AND T1.LEAD_CO_MNE = md.LEAD_CO_MNE -----ADD BY ANAS-----
  LEFT JOIN
            LIMIT L
         ON
            MD.CUSTOMER +'.' + LPAD (MD.LIMIT_REFERENCE , 10,'0000') = L.ID
        AND L.LEAD_CO_MNE                                            = CO_CHK.COMPANY_MNE
  LEFT JOIN
            (
             SELECT
                    LINE_CONTRACT                    ,
                    LEAD_CO_MNE                      ,
                    CURRENCY                         ,
                    MAX(crf.MAT_DATE)      AS MaturityDate,
                    MAX(att.Category)      AS Category   ,
                    MAX(crf.INTEREST_RATE) AS InterestRate,
                    SUM(
                        CASE
                            WHEN att.Category IS NOT NULL
                            THEN CAST(LOCAL_CCY_AMT AS DECIMAL (28,4))
                        END) AS Balance,
                    SUM(
                        CASE
                            WHEN att.Category IS NOT NULL
                            THEN CAST(CURRENCY_AMT AS DECIMAL (28,4))
                        END) AS ForeignCurrencyBal,
                    SUM(
                        CASE
                            WHEN substring(ASSET_TYPE,0, 1) = '5'
                            THEN CAST(LOCAL_CCY_AMT AS DECIMAL (28,4))
                        END) AS InterestAccrued,
                    SUM(
                        CASE
                            WHEN ASSET_TYPE    LIKE '%CONT%'
                            AND ASSET_TYPE NOT LIKE '%BL'
                            THEN CAST(LOCAL_CCY_AMT AS DECIMAL (28,4))
                        END)                                                         AS Authorized,
                    RANK() over ( partition BY LINE_CONTRACT ORDER BY LINE_CONTRACT) AS ColRank
               FROM
                    RE_CRF_GL crf
          LEFT JOIN
                    (
                     SELECT
                            'CONTDB' AssetType,
                            'LOAN'   Category           
                  UNION ALL
                     SELECT
                            'CONTDBBL' AssetType,
                            ''         Category
                  UNION ALL
                     SELECT
                            'CONTCR'  AssetType,
                            'DEPOSIT' Category
                  UNION ALL
                     SELECT
                            'CONTCRBL' AssetType,
                            'DEPOSIT' Category ) att
                 ON
                    att.AssetType = crf.ASSET_TYPE
           GROUP BY
                    LINE_CONTRACT,
                    CURRENCY     ,
                    LEAD_CO_MNE) gl
         ON
            gl.LINE_CONTRACT = md.ID
        AND gl.LEAD_CO_MNE   = com.FINANCIAL_MNE
  LEFT JOIN
            CUSTOMER custowner
         ON
            md.CUSTOMER           = custowner.ID
        AND custowner.LEAD_CO_MNE = com.CUSTOMER_MNEMONIC
LEFT JOIN
            MD_DEAL_LocalRef MDlf
         ON
            MDlf.ID          = MD.ID
        AND MDlf.LEAD_CO_MNE = MD.LEAD_CO_MNE 
  LEFT JOIN
            CATEGORY c
         ON
            c.ID           = md.CATEGORY
        AND md.LEAD_CO_MNE = c.LEAD_CO_MNE )
select a.*,a.mis_date as cob_date
from acc a