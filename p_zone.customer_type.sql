

drop table p_zone.customer_type
create external table p_zone.customer_type as 
with abc as
(
SELECT
	lead_co_mne,
	mis_date,
	id as src_id,
	description,
	short_name,
	co_code
FROM
	rz_dwexport.sector
)
select row_number() over(order by src_id) as customer_type_id, s.* from abc s 

