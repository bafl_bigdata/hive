CREATE VIEW p_zone.v_status_lov AS with abc as(
select
    customer_status.lead_co_mne as company ,
    customer_status.description as description ,
    customer_status.short_name as short_name ,
    customer_status.id as src_key ,
    '' as status ,
    '' as type ,
    'T24' as source_system
from
    rz_dwexport.customer_status
union all
select
    'BNK' ,
    status_setup.description ,
    '' ,
    cast(status_setup.id as string) ,
    status_setup.status ,
    status_setup.type ,
    'Banca'
from
    rz_banca.status_setup
union all
select
    'BNK' ,
    cardaccount_status.cardacctstatus_description ,
    '' ,
    cast(cardaccount_status.cardstatus_key as string) ,
    cardaccount_status.cardacctstatus_short_code ,
    cardaccount_status.cardacctstatus_type ,
    'CC'
from
    p_zone.cardaccount_status )
select
    row_number() OVER(
    order by abc.description )as id,
    abc.company,
    abc.description,
    abc.short_name,
    abc.src_key,
    abc.status,
    abc.type,
    abc.source_system
from
    abc
