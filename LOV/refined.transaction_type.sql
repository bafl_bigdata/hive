create table refined.transaction_type as 
with abc as (
select
	lead_co_mne ,
	sourcetranid ,
	tran_code ,
	tran_desc as description ,
	code ,
	tran_short_desc , 
	'T24' as source_system,
	credit_code ,
	debit_code ,
	event_name
from
	p_zone.transaction_type
where
	source_system = 'T24'
union all
select
	'BNK' ,
	m.trxn_type ,
	m.sys_trxn_cd ,
	m.description as description ,
	concat(m.trxn_type,	m.sys_trxn_cd) as code ,
	m.description as short_description , 
	'CC' ,
	null ,
	null ,
	'CC'
from
	rz_cc.sys_trnx_map m )
select
	*
from
	abc