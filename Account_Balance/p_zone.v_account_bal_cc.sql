CREATE or replace VIEW p_zone.v_account_bal_cc AS 
WITH abc AS (
SELECT 
	current_date as mis_date,
	'BNK' AS lead_co_mne,
	CASE WHEN ct.cb_cardholder_no LIKE '% %' THEN REPLACE(trim(ct.cb_cardholder_no),
	' ',
	'')
	ELSE trim(ct.cb_cardholder_no)
END AS account_num,
'PKR' as currency,
CASE WHEN ct.cb_idno LIKE '%-%' THEN REPLACE(trim(ct.cb_idno),
'-',
'')
ELSE trim(ct.cb_idno)
END AS customer_id,
cast(ind.cb_outstd_bal as decimal(23,5)) as balance_fcy,
cast(ind.cb_outstd_bal as decimal(23,5)) as balance_lcy,
CAST(from_unixtime(unix_timestamp(ct.cb_anniv_date,'yyyyMMdd'),'yyyy-MM-dd') AS DATE) as ac_opening_date,
ct.cb_embossname as account_title,
cast(gg.pr_prodct_group as string) as account_group,
'' as line_id,
cast(fin.cb_product_cd as string) as product_id,
concat('CP:',
'BNK',
':',
trim(ind.cb_individual_acctno)) as sourceaccountid,
'CC' AS source_system,
'' as interest_basis,
'' as interest_rate,
'' as rollover_date,
current_date as cob_date
FROM
rz_cardpro.cp_indacc AS ind
LEFT JOIN rz_cardpro.cp_crdtbl ct ON
ct.cb_individual_acctno = ind.cb_individual_acctno
LEFT JOIN rz_cardpro.cp_prdgrp gg ON
ct.cb_card_prdct_group = gg.pr_prodct_group
LEFT JOIN rz_cardpro.cp_fintbl AS fin ON
ind.cb_fin_acctno = fin.cb_fin_acctno 
)
select * from abc