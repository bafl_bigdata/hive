CREATE or replace VIEW p_zone.v_account_bal AS WITH bal AS (
SELECT 
	CAST(from_unixtime(unix_timestamp(rcb.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date,
	rcb.lead_co_mne AS lead_co_mne ,
	trim(rcb.line_or_contract) AS account_num ,
	rcb.currency AS currency ,
	rcb.customer AS customer_id ,
	cast(rcb.currency_amt as decimal(23,5)) AS balance_fcy,
	cast(rcb.local_ccy_amt as decimal(23,5)) AS balance_lcy,
	CAST(from_unixtime(unix_timestamp(rcb.ac_opening_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) AS ac_opening_date ,
	rcb.account_name AS account_title ,
	rcb.account_group AS account_group,
	rcb.line_id AS line_id ,
	rcb.category AS product_id,
	concat('BS:',
	rcb.lead_co_mne,
	':',
	trim(rcb.line_or_contract)) AS sourceaccountid,
	'T24' AS source_system,
	rcb.int_basis as interest_basis,
	rcb.interest_rate as interest_rate,
	rcb.rollover_date
FROM
	rz_dwexport.re_crf_balbs rcb	
UNION ALL
SELECT 
CAST(from_unixtime(unix_timestamp(rci.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date,
	rci.lead_co_mne ,
	trim(rci.line_or_contract) AS account_num ,
	rci.currency ,
	rci.customer ,
	cast(rci.currency_amt as decimal(18,4)) currency_amt ,
	cast(rci.local_ccy_amt as decimal(18,4)) local_ccy_amt ,
	CAST(from_unixtime(unix_timestamp(rci.ac_opening_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) ,
	rci.account_name ,
	rci.account_group ,
	rci.line_id ,
	rci.category,
	concat('BS:',
	rci.lead_co_mne,
	':',
	trim(rci.line_or_contract)) AS sourceaccountid,
	'T24',
	rci.int_basis as interest_basis,
	rci.interest_rate as interest_rate,
	rci.rollover_date
FROM
	rz_dwexport.re_crf_ibgbs rci
)
SELECT
	b.*,n.asl_type,n.prod_des,n.line_no_description,n.category_des,b.mis_date as cob_date
FROM
	bal b
left join p_zone.balbs_ibgbs_n n
on trim(b.line_id) = trim(n.line_cd)
and b.product_id=n.category_code