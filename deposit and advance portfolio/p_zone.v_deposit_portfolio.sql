create or replace view p_zone.v_deposit_portfolio as 
with abc as (
select
	ab.lead_co_mne ,
	ab.account_num ,
	ab.currency ,
	ab.customer_id ,
	CONCAT('BS:',ab.lead_co_mne,':',ab.customer_id) as sourcecustomerid ,
	ab.balance_fcy ,
	ab.balance_lcy ,
	ab.ac_opening_date ,
	ab.account_title ,
	ab.account_group ,
	ab.line_id ,
	ab.product_id ,
	ab.sourceaccountid ,
	ab.source_system ,
	ab.interest_basis ,
	ab.interest_rate ,
	from_unixtime(unix_timestamp(ab.rollover_date, 'yyyyMMdd'), 'yyyy-MM-dd') as rollover_date,
	acc.account_status ,
	ab.asl_type ,
	ab.cob_date
from
	p_zone.v_account_bal ab
left join p_zone.account acc on
	ab.account_num = acc.account_num
where
	ab.asl_type in('S',
	'T',
	'C',
	'O',
	'R',
	'P') )
select
	*
from
	abc