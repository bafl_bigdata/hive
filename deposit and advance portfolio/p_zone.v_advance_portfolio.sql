create or replace view p_zone.v_advance_portfolio as
select
	a.lead_co_mne ,
	a.account_num ,
	a.currency ,
	a.customer_id ,
	concat('BS:',a.lead_co_mne,':',a.customer_id) as sourcecustomerid ,
	a.balance_fcy ,
	a.balance_lcy ,
	a.ac_opening_date ,
	a.account_title ,
	a.account_group ,
	a.line_id ,
	a.product_id ,
	a.sourceaccountid ,
	a.source_system ,
	a.interest_basis ,
	a.interest_rate ,
	a.rollover_date ,
	acc.account_status ,
	a.asl_type,
	a.cob_date
from
	p_zone.v_account_bal a
left join p_zone.account acc on
	a.account_num = acc.account_num
where
	a.source_system = 'T24'	and a.asl_type = 'A' and acc.source_system='T24'
