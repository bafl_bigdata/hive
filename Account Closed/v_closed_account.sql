CREATE OR REPLACE VIEW p_zone.v_closed_account AS with abc AS (
select
	concat('BS:',
	cc.lead_co_mne,
	':',
	cc.id) as sourceaccountid,
	cc.id as account_num,
	CAST(from_unixtime(unix_timestamp(cc.acct_close_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as acct_close_date,
	cc.lead_co_mne ,
	acl.ac_categ as product_id,
	acl.reason,
	acl.creason,
	CAST(from_unixtime(unix_timestamp(cc.mis_date,'yyyyMMdd'),'yyyy-MM-dd') AS date) as mis_date,
	'T24' as source_system
from
	rz_dwexport.account_closed cc
left join rz_dwexport.account_closure ac on
	cc.id = ac.id
left join rz_dwexport.account_closure_localref acl on
	ac.id = acl.id 
)
select 
	a.*,a.mis_date as cob_date
from
	abc a