CREATE or replace VIEW p_zone.v_services as
with abc as (
select
	cast('T24-EStatement' as string )as service_name,
	p.id as account_num,
	concat('BS:',
	p.lead_co_mne,
	':',
	p.id) AS SourceAccountId,
	'' as id_number,
	p.acct_categ as product_id,
	p.acct_title as account_title,
	p.cust_cell as mobile_no,
	p.email as email,
	CAST(from_unixtime(unix_timestamp(p.request_date,
	'yyyyMMdd'))as date) as request_date,
	case
		when p.estatement = 'Y' then 'Activated'
		else 'Un-activated'
	end as status,
	p.sms as sms_flag,
	p.estatement as estmt_flag,
	lf.activation_type as activation_source,
	'T24' as source_system,
	CAST(from_unixtime(unix_timestamp(p.active_date,
	'yyyyMMdd'))as date) as active_date,
	CAST(from_unixtime(unix_timestamp(CAST('' as string),
	'yyyyMMdd'))as date) AS enroll_date,
	CAST(from_unixtime(unix_timestamp(CAST('' as string),
	'yyyyMMdd'))as date) as cancel_date,
	CAST(from_unixtime(unix_timestamp(p.mis_date,
	'yyyyMMdd'))as date) as mis_date
from
	rz_dwexport.pk_h_sms_cust_profile p
inner JOIN rz_dwexport.pk_h_sms_cust_profile_localref lf on
	p.id = lf.id
where
	p.estatement = 'Y'
union ALL
select
	cast('T24-SMS' as string) as service_name,
	p.id as account_num,
	concat('BS:',
	p.lead_co_mne,
	':',
	p.id) AS SourceAccountId,
	'' as id_number,
	p.acct_categ as product_id,
	p.acct_title as account_title,
	p.cust_cell as mobile_no,
	p.email as email,
	CAST(from_unixtime(unix_timestamp(p.request_date,
	'yyyyMMdd'))as date) as request_date,
	case
		when p.sms = 'Y' then 'Activated'
		else 'Un-activated'
end as status,
	p.sms as sms_flag,
	p.estatement as estmt_flag,
	lf.activation_type as activation_source,
	'T24' as source_system,
	CAST(from_unixtime(unix_timestamp(p.active_date,
	'yyyyMMdd'))as date) as active_date,
	CAST(from_unixtime(unix_timestamp(CAST('' as string),
	'yyyyMMdd'))as date) AS enroll_date,
	CAST(from_unixtime(unix_timestamp(CAST('' as string),
	'yyyyMMdd'))as date) as cancel_date,
	CAST(from_unixtime(unix_timestamp(p.mis_date,
	'yyyyMMdd'))as date) as mis_date
from
	rz_dwexport.pk_h_sms_cust_profile p
inner JOIN rz_dwexport.pk_h_sms_cust_profile_localref lf on
	p.id = lf.id
 )
select
	a.*,
	a.mis_date as cob_date
from
	abc a
