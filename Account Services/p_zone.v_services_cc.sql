create or replace view p_zone.v_services_cc as 
with abc as (
select
	cast('CC-SMS' as string) as service_name,
	trim(cs.card_no) as account_num,
	concat('BS:','BNK',':', tt.cb_individual_acctno) AS SourceAccountId,
	replace(trim(cs.nic),'-','') as id_number ,
	cast(ff.cb_product_cd as string) as product_id,
	tt.cb_embossname as account_title,
	cs.mobile as mobile_no,
	'' as email,
	--email
 CAST(from_unixtime(unix_timestamp(CAST('' as string),'yyyyMMdd'))as date) as request_date,
	--request_date
 'Activated' as status,
	--status
 'Y' as sms_flag,
	--sms_flag
 'N' as estmt_flag,
	--EStmt
 '' as activation_source,
	--activation source
 'CC' as source_system,
	--source_system
 CAST(from_unixtime(unix_timestamp(CAST('' as string),'yyyyMMdd'))as date) as active_date,
	CAST(from_unixtime(unix_timestamp(CAST(cs.enroll_date as string),'yyyyMMdd'))as date) as enroll_date,
	--needs to fix
 CAST(from_unixtime(unix_timestamp(CAST('' as string), 'yyyyMMdd'))as date) as cancel_date,
	CURRENT_DATE as mis_date,
	CURRENT_DATE as cob_date
from
	rz_cc.cp_sms cs
inner join rz_cardpro.cp_crdtbl tt on
	trim(cs.card_no) = trim(tt.cb_cardholder_no)
inner join rz_cardpro.cp_fintbl ff on
	tt.cb_individual_acctno = ff.cb_individual_acctno
union all
select
	cast('CC-EStatement' as string) as service_name,
	trim(es.card_no) as account_num,
	concat('BS:','BNK',	':',tt.cb_individual_acctno) AS SourceAccountId,
	replace(trim(es.nic),'-','') ,
	cast(ff.cb_product_cd as string),
	tt.cb_embossname as name,
	'',
	es.email ,
	--email
 CAST(from_unixtime(unix_timestamp(CAST('' as string),'yyyyMMdd'))as date) ,
	--request_date
 'Activated',
	--status
 'N',
	--sms_flag
 'Y',
	--EStmt
 '',
	--activation source
 'CC',
	--source_system
 CAST(from_unixtime(unix_timestamp(CAST('' as string),	'yyyyMMdd'))as date) as active_date,
	CAST(from_unixtime(unix_timestamp(CAST(es.enroll_date as string),'yyyyMMdd'),	'yyyy-MM-dd')as date) as enroll_date,
	--needs to fix
 cast(from_unixtime(unix_timestamp(CAST(es.cancel_date as string),'yyyyMMdd'),	'yyyy-MM-dd') as date) as cancel_date,
	CURRENT_DATE as mis_date
	,CURRENT_DATE
from
	rz_cc.cp_estmt es
inner join rz_cardpro.cp_crdtbl tt on
	trim(es.card_no) = trim(tt.cb_cardholder_no)
inner join rz_cardpro.cp_fintbl ff on
	tt.cb_individual_acctno = ff.cb_individual_acctno
	)
select * from abc