create external table p_zone.tbl_country as
with abc as (
select
	lead_co_mne as Company_Code ,
	id as src_key ,
	country_name ,
	short_name ,
	geographical_block,
	'T24' as source_system
from
	rz_dwexport.country
UNION ALL
select
	'BNK' ,
	cast(country_code_key as string) as src_key ,
	country_name ,
	country_short_code ,
	'',
	'CC' as source_system
from
	dp_mdb_layer_cc.dim_country )
select
	ROW_NUMBER() OVER (
order by
	src_key) as country_id,
	*
from
	abc