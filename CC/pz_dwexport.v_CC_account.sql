CREATE OR REPLACE VIEW p_zone.v_cc_account AS WITH cs AS (
SELECT
	CASE WHEN ct.cb_idno LIKE '%-%' THEN REPLACE(trim(ct.cb_idno),
	'-',
	'')
	ELSE trim(ct.cb_idno)
END AS Id_Number,
CASE WHEN ct.cb_idno LIKE '%-%' THEN REPLACE(trim(ct.cb_idno),
'-',
'')
ELSE trim(ct.cb_idno)
END AS customer_id,
CASE WHEN ct.cb_basic_customer_idno LIKE '%-%' THEN REPLACE(trim(ct.cb_basic_customer_idno),
'-',
'')
ELSE trim(ct.cb_basic_customer_idno)
END AS Basic_Id_Number ,
ct.cb_status_cd AS Account_status ,
a.cb_cardholder_name AS full_name,
CASE WHEN ct.cb_cardholder_no LIKE '% %' THEN REPLACE(trim(ct.cb_cardholder_no),
' ',
'')
ELSE trim(ct.cb_cardholder_no)
END AS Account_Num ,
ct.cb_basic_supp_ind AS Basic_Sup_Ind,
ct.cb_id_type AS Id_Type,
ct.cb_bsc_id_type AS Basic_Id_Type,
ct.cb_basic_cardholder_no AS Basic_CardNo,
ct.cb_individual_acctno AS Acct_No,
ct.cb_separate_stmt_ind ,
mc.er_epp_total_amt AS sbs_amt,
ct.cb_embossname AS account_title,
ct.cb_bill_cycle ,
ct.cb_autopay_ind ,
ct.cb_autopay_bank_branch ,
ct.cb_autopay_bank_accno ,
ct.cb_autopay_accname ,
ct.cb_frgn_autopay_ind ,
ct.cb_autopay_frgn_bank_branch ,
ct.cb_autopay_frgn_bank_accno ,
ct.cb_autopay_frgn_accname ,
ct.cb_pin_gen_ind ,
ct.cb_curr_bank_rel ,
ct.cb_centre_cd ,
ct.cb_source_cd ,
ct.cb_recommender_no ,
ct.cb_recommender_name ,
ct.cb_microfilm_no ,
CAST(from_unixtime(unix_timestamp(ct.cb_anniv_date,
'yyyyMMdd')) AS DATE) AS ac_open_dt ,
CAST(from_unixtime(unix_timestamp(ct.cb_anniv_date,
'yyyyMMdd')) AS DATE) AS cb_anniv_date,
ct.cb_card_prdct_group ,
ct.cb_fee_cd ,
ct.cb_credit_guarantee ,
ct.cb_renewal_flag AS renewal_flag,
CAST(from_unixtime(unix_timestamp(ct.cb_suspend_date,
'yyyyMMdd')) AS DATE) AS cb_suspend_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_bankruptcy_date,
'yyyyMMdd')) AS DATE) AS cb_bankruptcy_date ,
CASE WHEN ct.cb_cancel_date LIKE '%00000000%' THEN '00000000'
ELSE CAST(from_unixtime(unix_timestamp(ct.cb_cancel_date,
'yyyyMMdd')) AS DATE)
END AS cb_cancel_date ,
CASE WHEN ct.cb_legal_date LIKE '%00000000%' THEN '00000000'
ELSE CAST(from_unixtime(unix_timestamp(ct.cb_legal_date,
'yyyyMMdd')) AS DATE)
END AS cb_legal_date ,
CASE WHEN ct.cb_fraud_date LIKE '%00000000%' THEN '00000000'
ELSE CAST(from_unixtime(unix_timestamp(ct.cb_fraud_date,
'yyyyMMdd')) AS DATE)
END AS cb_fraud_date ,
CASE WHEN ct.cb_reserve_date LIKE '%00000000%' THEN '00000000'
ELSE CAST(from_unixtime(unix_timestamp(ct.cb_reserve_date,
'yyyyMMdd')) AS DATE )
END AS cb_reserve_date ,
CASE WHEN ct.cb_reinstate_date LIKE '%00000000%' THEN '00000000'
ELSE CAST(from_unixtime(unix_timestamp(ct.cb_reinstate_date,
'yyyyMMdd')) AS DATE)
END AS cb_reinstate_date ,
ct.cb_expiry_ccyymm ,
ct.cb_plastic_cd ,
CAST(from_unixtime(unix_timestamp(ct.cb_plastic_date,
'yyyyMMdd')) AS DATE) AS cb_plastic_date ,
ct.cb_4dbc ,
ct.cb_new_expiry_ccyymm ,
ct.cb_new_plastic_cd ,
CASE WHEN ct.cb_new_plastic_date LIKE '%00000000%' THEN '00000000'
ELSE CAST(from_unixtime(unix_timestamp(ct.cb_new_plastic_date,
'yyyyMMdd')) AS DATE)
END AS cb_new_plastic_date ,
ct.cb_new_4dbc ,
ct.cb_old_expiry_ccyymm ,
ct.cb_old_plastic_cd ,
ct.cb_xref_ind ,
ct.cb_xref_accno ,
ct.cb_user_field_num1 ,
ct.cb_user_field_num2 ,
ct.cb_user_field_num3 ,
ct.cb_user_field_num4 ,
ct.cb_user_code_1 ,
ct.cb_user_code_2 ,
ct.cb_user_code_3 ,
ct.cb_user_code_4 ,
CAST(from_unixtime(unix_timestamp(ct.cb_user_date_1,
'yyyyMMdd')) AS DATE) AS cb_user_date_1 ,
CAST(from_unixtime(unix_timestamp(ct.cb_user_date_2,
'yyyyMMdd')) AS DATE) AS cb_user_date_2 ,
ct.cb_user_amt_1 ,
ct.cb_user_amt_2 ,
ct.cb_user_field_1 ,
ct.cb_user_field_2 ,
ct.cb_user_field_3 ,
CAST(from_unixtime(unix_timestamp(ct.mod_date,
'yyyyMMdd')) AS DATE) AS mod_date ,
ct.mod_user ,
ct.cb_pin_offset ,
ct.cb_pvki ,
ct.cb_cic_cancel ,
ct.cb_crshld_ind ,
CASE WHEN ct.cb_crshld_enrol_date LIKE '%00000000%' THEN '00000000'
ELSE CAST(from_unixtime(unix_timestamp(ct.cb_crshld_enrol_date,
'yyyyMMdd')) AS DATE )
END AS cb_crshld_enrol_date ,
CASE WHEN ct.cb_crshld_terminate_date LIKE '%00000000%' THEN '00000000'
ELSE CAST(from_unixtime(unix_timestamp(ct.cb_crshld_terminate_date,
'yyyyMMdd') ) AS DATE)
END AS cb_crshld_terminate_date ,
ct.cb_corporate_id ,
ct.cb_sic ,
ct.cb_relationship ,
ct.cb_fraud_flag1 ,
ct.cb_fraud_flag2 ,
CAST(from_unixtime(unix_timestamp(ct.cb_first_use_date,
'yyyyMMdd')) AS DATE) AS cb_first_use_date ,
ct.cb_first_use_time ,
ct.cb_first_use_ind ,
ct.cb_old_id_type ,
ct.cb_cif_flag ,
ct.cb_dcms_flag ,
ct.cb_caf_refresh_flag ,
ct.cb_caf_update_flag ,
ct.cb_caf_pin_flag ,
ct.cb_caf_tag_flag ,
ct.cb_card_coll_method ,
ct.cb_card_delivery_addr ,
ct.cb_bill_addr_cd ,
ct.cb_rwds_accno ,
ct.cb_legal_addr_cd ,
ct.cb_last_renew_fee_year ,
CAST(from_unixtime(unix_timestamp(ct.cb_last_renewal_date,
'yyyyMMdd')) AS DATE) AS cb_last_renewal_date ,
ct.cb_monitor_cd ,
ct.cb_photo_ind ,
ct.cb_prd_category ,
ct.cb_last_usr_trans_code ,
CAST(from_unixtime(unix_timestamp(ct.cb_last_usr_trans_date,
'yyyyMMdd')) AS DATE) AS last_cr_cust_date,
ct.cb_last_sys_trans_code ,
CAST(from_unixtime(unix_timestamp(ct.cb_last_sys_trans_date,
'yyyyMMdd')) AS DATE) AS last_dr_cust_date ,
ct.cb_min_pmt_rate ,
ct.cb_vip_auth_bypass ,
ct.cb_vip_ref_name ,
ct.cb_vip_ref_telno ,
ct.cb_stip_status ,
ct.cb_stip_reason_code ,
CAST(from_unixtime(unix_timestamp(ct.cb_last_stip_date,
'yyyyMMdd')) AS DATE) AS cb_last_stip_date ,
ct.cb_stmt_hold_ind ,
ct.cb_gift_cd ,
ct.cb_hi_balance ,
CAST(from_unixtime(unix_timestamp(ct.cb_curr_status_chg_date,
'yyyyMMdd')) AS DATE) AS cb_curr_status_chg_date ,
ct.cb_prev_status_cd ,
CAST(from_unixtime(unix_timestamp(ct.cb_prev_status_date,
'yyyyMMdd')) AS DATE) AS cb_prev_status_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_applcation_date,
'yyyyMMdd')) AS DATE) AS cb_applcation_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_application_appv_date,
'yyyyMMdd')) AS DATE) AS cb_application_appv_date ,
ct.cb_appv_user_id ,
CAST(from_unixtime(unix_timestamp(ct.cb_embossed_date,
'yyyyMMdd')) AS DATE) AS cb_embossed_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_plastic_coll_date,
'yyyyMMdd')) AS DATE) AS cb_plastic_coll_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_acc_close_date,
'yyyyMMdd')) AS DATE) AS cb_acc_close_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_decease_date,
'yyyyMMdd')) AS DATE) AS cb_decease_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_inactive_date,
'yyyyMMdd')) AS DATE) AS cb_inactive_date ,
ct.cb_npl_status ,
CAST(from_unixtime(unix_timestamp(ct.cb_curr_npl_date,
'yyyyMMdd')) AS DATE) AS cb_curr_npl_date ,
ct.cb_prev_npl_status ,
CAST(from_unixtime(unix_timestamp(ct.cb_prev_npl_date,
'yyyyMMdd')) AS DATE) AS cb_prev_npl_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_charge_off_date,
'yyyyMMdd')) AS DATE) AS cb_charge_off_date ,
ct.cb_charge_off_reason ,
CAST(from_unixtime(unix_timestamp(ct.cb_write_off_date,
'yyyyMMdd')) AS DATE) AS cb_write_off_date ,
ct.cb_write_off_reason ,
ct.cb_date_into_coll ,
ct.cb_coll_reason ,
ct.cb_profile_hist ,
ct.checksum ,
CAST(from_unixtime(unix_timestamp(ct.cb_reaged_date,
'yyyyMMdd')) AS DATE) AS cb_reaged_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_pin_chg_date,
'yyyyMMdd')) AS DATE) AS cb_pin_chg_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_crlimit_chg_date,
'yyyyMMdd')) AS DATE) AS cb_crlimit_chg_date ,
ct.cb_cobrand_id ,
ct.cb_embossname2 ,
CAST(from_unixtime(unix_timestamp(ct.cb_last_in_coll_date,
'yyyyMMdd')) AS DATE) AS cb_last_in_coll_date ,
ct.cb_in_coll_count ,
CAST(from_unixtime(unix_timestamp(ct.cb_gift_recovery_date,
'yyyyMMdd')) AS DATE ) AS cb_gift_recovery_date ,
ct.cb_mkt_program_code ,
ct.cb_crshld_source_cd ,
ct.cb_crshld_package_id ,
ct.cb_crshld_source_ind ,
CAST(from_unixtime(unix_timestamp(ct.cb_first_activated_date,
'yyyyMMdd')) AS DATE) AS cb_first_activated_date ,
ct.cb_daily_cash_limit ,
ct.cb_daily_noncash_limit ,
ct.cb_emboss_ind ,
ct.cb_frgn_autopay_rate ,
ct.cb_autopay_rate ,
ct.cb_appl_no ,
ct.cb_internet_ind ,
CAST(from_unixtime(unix_timestamp(ct.cb_internet_start_date,
'yyyyMMdd')) AS DATE) AS cb_internet_start_date ,
CAST(from_unixtime(unix_timestamp(ct.cb_internet_end_date,
'yyyyMMdd')) AS DATE) AS cb_internet_end_date ,
ct.cb_internet_start_time ,
ct.cb_internet_end_time ,
ct.cb_sms_subscribe_ind ,
ct.cb_plastic_face ,
ct.cb_cash_ind ,
ct.cb_deviation_cd ,
CAST(from_unixtime(unix_timestamp(CAST(ct.cb_cutoff_date AS string),
'yyyyMMdd')) AS DATE) AS cb_cutoff_date ,
ct.cb_cutoff_time ,
ct.cb_cross_border_usg ,
ct.cb_excess_ovr_lmt ,
ct.cb_contactless_atc ,
gg.pr_card_brand AS pr_card_brand,
gg.pr_category AS category,
ct.cb_plastic_cd AS plastic_cd,
ct.cb_monitor_cd AS monitor_cd ,
trim(ct.cb_cardholder_no) AS card_no,
ind.cb_line_limit AS `limit`,
ind.cb_outstd_bal AS balance_lcy ,
ind.cb_outstd_bal AS balance_fcy ,
a.cb_cust_status_cd AS status_cd ,
fin.cb_product_cd AS ac_category ,
fin.cb_product_cd AS product_id,
'CC' AS source_system,
ac.surr_account_id,
gg.pr_prodct_group,
gg.pr_group_desc,
CASE WHEN ct.cb_renewal_flag = 'W' THEN 'WITHHELD'
WHEN ct.cb_renewal_flag = 'NA' THEN 'NOT AVAILABLE'
WHEN ct.cb_renewal_flag = 'NW' THEN 'NON-WITHHELD'
END AS RenewalFlagDesc,
CASE WHEN trim(ct.cb_basic_supp_ind) = 'B' THEN 'BASIC'
WHEN trim(ct.cb_basic_supp_ind) = 'S' THEN 'SUPP'
END AS BASIC_SUPP_FLAG_DESC,
'0050' AS branch_code,
CASE WHEN COALESCE(REPLACE(ct.cb_status_cd,
' ',
''),
'') = '' THEN 'ACTIVE'
WHEN trim(REPLACE(ct.cb_status_cd, ' ', '')) = 'R' THEN 'Payrol Card'
WHEN trim(REPLACE(ct.cb_status_cd, ' ', '')) = 'V' THEN 'Cancelled by Customer'
WHEN trim(REPLACE(ct.cb_status_cd, ' ', '')) = 'L' THEN 'Legal'
WHEN trim(REPLACE(ct.cb_status_cd, ' ', '')) = 'I' THEN 'N/A'
WHEN trim(REPLACE(ct.cb_status_cd, ' ', '')) = 'D' THEN 'Deceased'
WHEN trim(REPLACE(ct.cb_status_cd, ' ', '')) = 'N' THEN 'NPL'
WHEN trim(REPLACE(ct.cb_status_cd, ' ', '')) = 'F' THEN 'Fraud'
WHEN trim(REPLACE(ct.cb_status_cd, ' ', '')) = 'C' THEN 'Cancelled by Bank'
WHEN trim(REPLACE(ct.cb_status_cd, ' ', '')) = 'G' THEN 'Unknown'
WHEN trim(REPLACE(ct.cb_status_cd, ' ', '')) = 'S' THEN 'Collection Case'
ELSE 'None'
END AS Acct_Card_Status_Desc ,
fin.cb_fin_acctno,
trim(ct.cb_individual_acctno) as ind_acct_no ,
fin.cb_fin_acctno as fin_acct_no,
case
when trim(ct.cb_cardholder_no) like '4%' then 'VISA'
when trim(ct.cb_cardholder_no) like '5%' then 'MASTER'
when trim(ct.cb_cardholder_no) like '3%' then 'AMEX'
else 'OTHER'
end as Brand,
'CP' as sourceapplication,
concat('CP:',
'BNK',
':',
trim(ct.cb_individual_acctno)) as sourceaccountid
FROM
rz_cardpro.cp_crdtbl ct
LEFT JOIN rz_cardpro.cp_csttbl AS a ON
trim(ct.cb_idno) = trim(a.cb_customer_idno)
LEFT JOIN rz_cardpro.cp_mcdisr AS mc ON
ct.cb_individual_acctno = trim(mc.er_cardhdr_no)
LEFT JOIN rz_cardpro.cp_indacc AS ind ON
ct.cb_individual_acctno = ind.cb_individual_acctno
LEFT JOIN rz_cardpro.cp_fintbl AS fin ON
ind.cb_fin_acctno = fin.cb_fin_acctno
LEFT JOIN rz_cardpro.cp_prdgrp gg ON
ct.cb_card_prdct_group = gg.pr_prodct_group
LEFT JOIN p_zone.surr_account AS ac ON
concat('CP:',
'BNK',
':',
trim(ct.cb_individual_acctno)) = ac.sourceaccountid )
SELECT
	concat('CP:',
	'BNK',
	':',
	trim(c.customer_id)) as sourcecustomerid,
	c.*,
	current_date as cob_date
FROM
	cs AS c